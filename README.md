# The Invisible Labyrinth

In *The Invisible Labyrinth*, you and others enter a randomly generated **labyrinth**. Each turn, everyone rolls a die to see how far they can move. The first to reach the exit wins. However, there's a catch: **All the walls are invisible**!

When making a turn, you enter all the steps you want to do at once, without the other players being able to see where you are going. Then, all steps are executed, but the moment you hit a wall, your turn ends immediately. All that is made public is the number you rolled, where you started and where you ended your turn. The same goes for the other players. Explore the labyrinth, memorize known paths and walls and deduce information from the other players' results.

You can also play against AI with a customizable strength setting ranging from completely random to using Bayesian statistics to infer the locations of walls and using A* pathfinding to decide their moves. To see how the AI works, let it play against itself and you will see where it thinks the walls are.

Note that the game was developed in January/February 2019 over the course of one week - the dates in this public repository differ because of rebasing.

## Controls

**WASD**: move player  
**Backspace**: undo move  
**Enter**: start turn, end turn  
**M**: back to menu  
**Arrow keys**: change window size