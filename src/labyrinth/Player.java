package labyrinth;

import labyrinth.controller.AIPlayer;
import labyrinth.controller.PlayerController;

public class Player {
    private int color;
    private PlayerController controller;
    private Game game;
    private Coordinate position;
    private Coordinate lastPosition;
    private int diceRoll;

    Player(int color, PlayerController controller) {
        this.color = color;
        this.controller = controller;
    }

    Player(int color, float aiStrength) {
        this.color = color;
        this.controller = AIPlayer.fromStrength(this, aiStrength);
    }

    Player(int color, float bayesFactor, float bayesCutoff) {
        this.color = color;
        this.controller = AIPlayer.bayes(this, bayesFactor, bayesCutoff);
    }

    public int getColor() {
        return color;
    }

    public PlayerController getController() {
        return controller;
    }

    public Game getGame() {
        return game;
    }

    public Coordinate getPosition() {
        return position;
    }

    public Coordinate getLastPosition() {
        return lastPosition;
    }

    public int getDiceRoll() {
        return diceRoll;
    }

    void setGame(Game game) {
        this.game = game;
    }

    void setPosition(Coordinate position) {
        this.lastPosition = this.position == null ? position : this.position;
        this.position = position;
    }

    public void setDiceRoll(int diceRoll) {
        this.diceRoll = diceRoll;
    }
}
