package labyrinth;

public class Wall {
    private Coordinate position;
    private boolean isHorizontal;

    public Wall(Coordinate position, boolean isHorizontal) {
        this.position = position;
        this.isHorizontal = isHorizontal;
    }

    public Coordinate getPosition() {
        return position;
    };

    public int getX() {
        return position.getX();
    }

    public int getY() {
        return position.getY();
    }

    public boolean isHorizontal() {
        return isHorizontal;
    }

    @Override
    public boolean equals(Object obj) {
        if (! (obj instanceof Wall)) {
            return false;
        }
        Wall o = (Wall) obj;
        return o.position.equals(position) && o.isHorizontal == isHorizontal;
    }

    @Override
    public int hashCode() {
        return position.hashCode()*2 + (isHorizontal ? 1 : 0);
    }
}
