package labyrinth.controller.ai;

import labyrinth.Coordinate;
import labyrinth.Direction;
import labyrinth.Player;
import labyrinth.Wall;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class BayesStarAI extends AStar4AI {
    private HashMap<Wall, Float> probabilities;
    private float bayesFactor;
    private float bayesCutoff;

    public BayesStarAI(Player player) {
        super(player);

        probabilities = new HashMap<>();
        bayesFactor = 4;
        bayesCutoff = 0.5f;
    }

    public BayesStarAI(Player player, float bayesFactor, float bayesCutoff) {
        super(player);

        probabilities = new HashMap<>();
        this.bayesFactor = bayesFactor;
        this.bayesCutoff = bayesCutoff;
    }

    private void makeProbabilities() {
        int gridWidth = 0;
        while(getPlayer().getGame().insideBounds(new Coordinate(gridWidth, 0))) {
            gridWidth++;
        }

        int gridHeight = 0;
        while(getPlayer().getGame().insideBounds(new Coordinate(0, gridHeight))) {
            gridHeight++;
        }

        final float[] aPriori = calculateAPriori(gridWidth, gridHeight);

        for (int row = 0; row < gridHeight; row++) {
            for (int col = 0; col < gridWidth; col++) {
                Coordinate position = new Coordinate(col, row);
                if (col < gridWidth-1) {
                    probabilities.put(Direction.RIGHT.getWall(position),
                            aPriori[row == 0 || row == gridHeight - 1 ? 0 : 1]);
                }
                if (row < gridHeight-1) {
                    probabilities.put(Direction.DOWN.getWall(position),
                            aPriori[col == 0 || col == gridWidth - 1 ? 0 : 1]);
                }
            }
        }
    }

    private float[] calculateAPriori(int gridWidth, int gridHeight) {
        final float wallFactor = getPlayer().getGame().getWallFactor();

        final int allPossibleWalls = (gridWidth-1)*gridHeight + (gridHeight-1)*gridWidth;
        final int possibleOuterWalls = 2*(gridHeight+gridWidth-2);
        final int maximumWalls = (gridWidth-1)*(gridHeight-1);

        final float actualWallsEstimate = (float) (maximumWalls * Math.tanh(
                Math.exp( wallFactor * wallFactor * 3.24 - 0.18 )));
        final float minOuterWallsEstimate = 0.18f * possibleOuterWalls;
        final float maxOuterWallsEstimate = (float) maximumWalls
                * possibleOuterWalls / allPossibleWalls;
        final float outerWallsEstimate = minOuterWallsEstimate
                + wallFactor * (maxOuterWallsEstimate - minOuterWallsEstimate);

        final float outerProbability = outerWallsEstimate/possibleOuterWalls;
        final float innerProbability = (actualWallsEstimate - outerWallsEstimate)
                / (allPossibleWalls - possibleOuterWalls);

        return new float[] {outerProbability, innerProbability};
    }

    @Override
    public float getCost(Coordinate position, Direction move) {
        if (! probabilities.containsKey(move.getWall(position))) {
            return super.getCost(position, move);
        }
        float prob = probabilities.get(move.getWall(position));
        float added = prob > bayesCutoff ? bayesFactor*(prob - bayesCutoff) : 0;
        return super.getCost(position, move) + added;
    }

    @Override
    public ArrayList<Direction> getPath(Coordinate position, int diceRoll) {
        if (probabilities.size() == 0) {
            makeProbabilities();
        }
        return super.getPath(position, diceRoll);
    }

    @Override
    void addKnownWall(Wall wall) {
        super.addKnownWall(wall);
        probabilities.put(wall, 1f);
    }

    @Override
    void addKnownPath(Wall wall) {
        super.addKnownPath(wall);
        probabilities.put(wall, 0f);
    }

    @Override
    public void log(Player player, int diceRoll, Coordinate originalPosition, Coordinate newPosition) {
        if (probabilities.size() == 0) {
            makeProbabilities();
        }

        super.log(player, diceRoll, originalPosition, newPosition);

        if (player != getPlayer()) {
            updateProbabilities(diceRoll, originalPosition, newPosition);
        }
    }

    @Override
    void scanCorners() {
        for (int col = 1; getPlayer().getGame().insideBounds(new Coordinate(col, 0)); col++) {
            for (int row = 1; getPlayer().getGame().insideBounds(new Coordinate(col, row)); row++) {
                HashSet<Wall> walls = scanCorner(col, row);
                float wallSum = 0;
                for (Wall wall : walls) {
                    wallSum += probabilities.get(wall);
                }
                if (wallSum < 1) {
                    for (Wall wall : walls) {
                        probabilities.put(wall, probabilities.get(wall)/wallSum);
                    }
                }
            }
        }
    }

    @Override
    void scanTiles() {
        for (int col = 0; getPlayer().getGame().insideBounds(new Coordinate(col, 0)); col++) {
            for (int row = 0; getPlayer().getGame().insideBounds(new Coordinate(col, row)); row++) {
                HashSet<Wall> walls = scanTile(col, row);
                float wallSum = 0;
                for (Wall wall : walls) {
                    wallSum += 1 - probabilities.get(wall);
                }
                if (wallSum < 1) {
                    for (Wall wall : walls) {
                        float inverseProbability = 1-probabilities.get(wall);
                        inverseProbability /= wallSum;
                        probabilities.put(wall, 1-inverseProbability);
                    }
                }
            }
        }
    }

    private void collectRelevantWalls(int remaining, Coordinate position, HashSet<Wall> relevantWalls) {
        if (remaining > 0) {
            for (Direction move : Direction.values()) {
                Coordinate target = move.getTarget(position);
                if (getPlayer().getGame().insideBounds(target)) {
                    relevantWalls.add(move.getWall(position));
                    collectRelevantWalls(remaining-1, target, relevantWalls);
                }
            }
        }
    }

    private double getTargetProbability(int remaining, Coordinate position,
                                        Coordinate goal, Wall sureWall) {
        if (position.equals(goal)) {
            return 1;
        }
        if (remaining == 0) {
            return 0;
        }

        double probability = 0;
        HashSet<Wall> knownWalls = getKnownWalls();

        for (Direction move : Direction.values()) {
            Coordinate target = move.getTarget(position);
            Wall wall = move.getWall(position);

            if (! getPlayer().getGame().insideBounds(target)) {
                continue;
            }
            if (knownWalls.contains(wall)) {
                continue;
            }

            double wallProbability = wall.equals(sureWall) ? 1 : probabilities.get(wall);
            probability += (1 - wallProbability) * getTargetProbability(
                    remaining-1, target, goal, sureWall);
        }

        return probability;
    }

    private void updateProbabilities(int diceRoll, Coordinate originalPosition,
                                     Coordinate newPosition) {
        HashSet<Wall> relevantWalls = new HashSet<>();
        collectRelevantWalls(diceRoll, originalPosition, relevantWalls);

        HashMap<Wall, Float> newProbabilities = new HashMap<>();

        double targetProbability = getTargetProbability(diceRoll, originalPosition,
                newPosition, null);

        for (Wall wall : relevantWalls) {
            double probability = getTargetProbability(diceRoll, originalPosition,
                    newPosition, wall) * probabilities.get(wall) / targetProbability;
            newProbabilities.put(wall, (float) probability);
        }

        HashSet<Wall> knownWalls = getKnownWalls();
        HashSet<Wall> knownPaths = getKnownPaths();

        for (Map.Entry<Wall, Float> entry : newProbabilities.entrySet()) {
            if (! knownWalls.contains(entry.getKey()) && ! knownPaths.contains(entry.getKey())) {
                probabilities.put(entry.getKey(), entry.getValue());
            }
        }
    }

    @Override
    public int getColor(Wall wall) {
        if (getKnownWalls().contains(wall) || getKnownPaths().contains(wall)) {
            return super.getColor(wall);
        }
        if (probabilities.containsKey(wall)) {
            float prob = probabilities.get(wall);
            if (prob < 0.25f) {
                return -256 - 65536 * (int) (prob*255*4);
            } else if (prob < 0.5f) {
                return -16711936 + (int) ((prob-0.25f)*255*4);
            } else {
                return -16711681 - 256 * (int) ((prob-0.5f)*255*2);
            }
        } else {
            return 127;
        }
    }
}
