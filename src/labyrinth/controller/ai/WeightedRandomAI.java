package labyrinth.controller.ai;

import labyrinth.Coordinate;
import labyrinth.Direction;
import labyrinth.Player;
import labyrinth.Wall;

import java.util.ArrayList;

public class WeightedRandomAI implements AI {
    private Player player;

    public WeightedRandomAI(Player player) {
        this.player = player;
    }

    Player getPlayer() {
        return player;
    }

    float getMoveWeight(Coordinate position, Direction move) {
        int finishDistance = player.getGame().getFinishDistance(position);
        int newFinishDistance = player.getGame().getFinishDistance(move.getTarget(position));
        return newFinishDistance < finishDistance ? 3 : 1;
    }

    @Override
    public ArrayList<Direction> getPath(Coordinate position, int diceRoll) {
        ArrayList<Direction> path = new ArrayList<>();
        for (int i = 0; i < diceRoll; i++) {
            Direction move = getMove(position);
            position = move.getTarget(position);
            path.add(move);
        }
        return path;
    }

    private Direction getMove(Coordinate position) {
        ArrayList<Direction> moves = new ArrayList<>();
        ArrayList<Float> weights = new ArrayList<>();

        for (Direction move : Direction.values()) {
            Coordinate target = move.getTarget(position);

            if (player.getGame().insideBounds(position) && ! player.getGame().insideBounds(target)) {
                continue;
            }

            moves.add(move);
            weights.add(getMoveWeight(position, move));
        }

        return WeightedRandom.choice(moves, weights);
    }

    @Override
    public void log(ArrayList<Direction> move) {

    }

    @Override
    public void log(Player player, int diceRoll, Coordinate originalPosition, Coordinate newPosition) {

    }

    @Override
    public int getColor(Wall wall) {
        return 255-64;
    }
}
