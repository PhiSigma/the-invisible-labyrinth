package labyrinth.controller.ai;

import labyrinth.*;

import java.util.ArrayList;
import java.util.HashSet;

public class AStar4AI extends AStarAI {
    private HashSet<Wall> knownPaths;

    public AStar4AI(Player player) {
        super(player);
        knownPaths = new HashSet<>();
    }

    HashSet<Wall> getKnownPaths() {
        return knownPaths;
    }

    void addKnownPath(Wall wall) {
        knownPaths.add(wall);
    }

    @Override
    public void log(Player player, int diceRoll, Coordinate originalPosition, Coordinate newPosition) {
        super.log(player, diceRoll, originalPosition, newPosition);

        final ArrayList<Direction> lastMove = getLastMove();

        if (player != getPlayer()) {
            if (diceRoll == 1 && ! newPosition.equals(originalPosition)) {
                Direction move = Direction.getMove(originalPosition, newPosition);
                if (move != null) {
                    addKnownPath(move.getWall(originalPosition));
                }
            }
            return;
        }

        if (lastMove == null || hasLoop(lastMove, originalPosition)) {
            return;
        }

        Coordinate position = originalPosition;
        for (Direction move : lastMove) {
            if (! position.equals(newPosition)) {
                addKnownPath(move.getWall(position));
                position = move.getTarget(position);
            } else {
                break;
            }
        }

        scanCorners();
        scanTiles();
    }

    HashSet<Wall> scanCorner(int col, int row) {
        HashSet<Wall> walls = new HashSet<>() {{
                add(new Wall(new Coordinate(col - 1, row), true));
                add(new Wall(new Coordinate(col, row - 1), false));
                add(new Wall(new Coordinate(col, row), true));
                add(new Wall(new Coordinate(col, row), false));
            }};

        Wall missing = null;

        for (Wall wall : walls) {
            if (! knownPaths.contains(wall)) {
                if (missing != null) {
                    return walls;
                }
                missing = wall;
            }
        }

        if (missing != null) {
            addKnownWall(missing);
        }

        return walls;
    }

    HashSet<Wall> scanTile(int col, int row) {
        Coordinate position = new Coordinate(col, row);
        HashSet<Wall> walls = new HashSet<>();
        Wall missing = null;
        HashSet<Wall> knownWalls = getKnownWalls();
        Game game = getPlayer().getGame();
        boolean atMostOne = true;

        for (Direction move : Direction.values()) {
            Wall wall = move.getWall(position);
            if (game.insideBounds(move.getTarget(position)) && ! knownWalls.contains(wall)) {
                walls.add(wall);
                if (missing != null) {
                    atMostOne = false;
                    break;
                }
                missing = wall;
            }
        }

        if (missing != null && atMostOne) {
            addKnownPath(missing);
        }

        return walls;
    }

    void scanCorners() {
        for (int col = 1; getPlayer().getGame().insideBounds(new Coordinate(col, 0)); col++) {
            for (int row = 1; getPlayer().getGame().insideBounds(new Coordinate(col, row)); row++) {
                scanCorner(col, row);
            }
        }
    }

    void scanTiles() {
        for (int col = 0; getPlayer().getGame().insideBounds(new Coordinate(col, 0)); col++) {
            for (int row = 0; getPlayer().getGame().insideBounds(new Coordinate(col, row)); row++) {
                scanTile(col, row);
            }
        }
    }

    private boolean hasLoop(ArrayList<Direction> moves, Coordinate originalPosition) {
        HashSet<Coordinate> positions = new HashSet<>();
        Coordinate position = originalPosition;
        positions.add(position);
        for (Direction move : moves) {
            position = move.getTarget(position);
            if (positions.contains(position)) {
                return true;
            }
            positions.add(position);
        }
        return false;
    }

    @Override
    public int getColor(Wall wall) {
        if (getKnownPaths().contains(wall)) {
            return 255;
        } else if (getKnownWalls().contains(wall)) {
            return 0;
        } else {
            return 127;
        }
    }
}
