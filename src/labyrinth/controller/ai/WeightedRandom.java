package labyrinth.controller.ai;

import java.util.List;

class WeightedRandom {
    static <T> T choice(List<T> options, List<Float> weights) {
        if (options == null || options.size() == 0) {
            throw new IllegalArgumentException("No options for weighted random choice");
        }

        if (weights.size() != options.size()) {
            throw new IllegalArgumentException("Not enough weights for weighted random choice");
        }

        float weightSum = 0;
        for (float weight : weights) {
            weightSum += weight;
        }

        double rand = Math.random()*weightSum;

        weightSum = 0;
        for (int i = 0; i < options.size(); i++) {
            weightSum += weights.get(i);
            if (rand < weightSum) {
                return options.get(i);
            }
        }

        return options.get(options.size()-1);
    }
}
