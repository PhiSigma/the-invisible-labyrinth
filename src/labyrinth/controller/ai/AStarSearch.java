package labyrinth.controller.ai;

import labyrinth.Coordinate;
import labyrinth.Direction;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeSet;

class AStarSearch {
    private TreeSet<PriorityNode> openList;
    private HashSet<Coordinate> closedList;
    private AStarEnvironment environment;

    AStarSearch(Coordinate startPosition, AStarEnvironment environment) {
        openList = new TreeSet<>();
        openList.add(new PriorityNode(startPosition, 0, 0));
        closedList = new HashSet<>();
        this.environment = environment;
    }

    private void collectMoves(PriorityNode node, ArrayList<Direction> path) {
        ArrayList<PriorityNode> parents = node.getParents();
        if (parents.size() > 0) {
            PriorityNode parent = parents.get((int) (node.getParents().size() * Math.random()));
            path.add(0, Direction.getMove(parent.getPosition(), node.getPosition()));
            collectMoves(parent, path);
        }
    }

    private ArrayList<Direction> getPath(PriorityNode finalNode) {
        ArrayList<Direction> path = new ArrayList<>();
        collectMoves(finalNode, path);
        return path;
    }

    ArrayList<Direction> getPath() {
        do {

            PriorityNode currentNode = openList.first();
            openList.remove(currentNode);

            if (environment.isGoal(currentNode.getPosition())) {
                return getPath(currentNode);
            }

            closedList.add(currentNode.getPosition());
            expandNode(currentNode, openList, closedList);

        } while (openList.size() > 0);

        return new ArrayList<>();
    }

    private void expandNode(PriorityNode currentNode, TreeSet<PriorityNode> openList,
                            HashSet<Coordinate> closedList) {
        for (Direction move : environment.getDirections(currentNode)) {

            Coordinate position = currentNode.getPosition();
            Coordinate target = move.getTarget(position);
            if (closedList.contains(target)) {
                continue;
            }

            float g = currentNode.getG() + environment.getCost(position, move);
            float f = g + environment.heuristic(target);

            PriorityNode successor = new PriorityNode(target, f, g);

            if (openList.contains(successor)) {

                boolean skipThis = false;

                for (PriorityNode otherNode : openList) {
                    if (otherNode.equals(successor)) {
                        float otherG = otherNode.getG();
                        if (otherG <= g) {
                            skipThis = true;
                            if (otherG == g) {
                                otherNode.addParent(currentNode);
                            }
                        } else {
                            openList.remove(otherNode);
                        }
                        break;
                    }
                }

                if (skipThis) {
                    continue;
                }

            }

            successor.addParent(currentNode);
            openList.add(successor);
        }
    }
}
