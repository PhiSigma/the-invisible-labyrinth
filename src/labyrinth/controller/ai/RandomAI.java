package labyrinth.controller.ai;

import labyrinth.Coordinate;
import labyrinth.Direction;
import labyrinth.Player;
import labyrinth.Wall;

import java.util.ArrayList;
import java.util.Arrays;

public class RandomAI implements AI {
    public static Direction randomMove() {
        return WeightedRandom.choice(Arrays.asList(Direction.values()), Arrays.asList(1f, 1f, 1f, 1f));
    }

    @Override
    public ArrayList<Direction> getPath(Coordinate position, int diceRoll) {
        ArrayList<Direction> path = new ArrayList<>();
        for (int i = 0; i < diceRoll; i++) {
            path.add(randomMove());
        }
        return path;
    }

    @Override
    public void log(ArrayList<Direction> move) {

    }

    @Override
    public void log(Player player, int diceRoll, Coordinate originalPosition, Coordinate newPosition) {

    }

    @Override
    public int getColor(Wall wall) {
        return 255-64;
    }
}
