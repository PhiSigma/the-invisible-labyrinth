package labyrinth.controller.ai;

import labyrinth.Coordinate;
import labyrinth.Direction;
import labyrinth.Player;

import java.util.ArrayList;
import java.util.List;

public class AStarAI extends RememberWallsAI implements AStarEnvironment {
    public AStarAI(Player player) {
        super(player);
    }

    @Override
    public ArrayList<Direction> getPath(Coordinate position, int diceRoll) {
        return new AStarSearch(position, this).getPath();
    }

    @Override
    public boolean isGoal(Coordinate position) {
        return getPlayer().getGame().getFinishDistance(position) == 0;
    }

    @Override
    public List<Direction> getDirections(PriorityNode currentNode) {
        ArrayList<Direction> directions = new ArrayList<>();
        for (Direction move : Direction.values()) {
            final Coordinate position = currentNode.getPosition();

            if (getPlayer().getGame().insideBounds(move.getTarget(position))
                    && ! getKnownWalls().contains(move.getWall(position))) {
                directions.add(move);
            }
        }
        return directions;
    }

    @Override
    public float heuristic(Coordinate position) {
        return getPlayer().getGame().getFinishDistance(position);
    }

    @Override
    public float getCost(Coordinate position, Direction move) {
        return 1;
    }
}
