package labyrinth.controller.ai;

import labyrinth.Coordinate;
import labyrinth.Direction;
import labyrinth.Player;
import labyrinth.Wall;
import labyrinth.controller.AIPlayer;
import labyrinth.controller.PlayerController;

import java.util.ArrayList;
import java.util.HashSet;

public class RememberWallsAI extends WeightedRandomAI {
    private HashSet<Wall> knownWalls;
    private ArrayList<Direction> lastMove;

    public RememberWallsAI(Player player) {
        super(player);
        knownWalls = new HashSet<>();
    }

    HashSet<Wall> getKnownWalls() {
        return knownWalls;
    }

    void addKnownWall(Wall wall) {
        knownWalls.add(wall);
    }

    ArrayList<Direction> getLastMove() {
        return lastMove;
    }

    @Override
    float getMoveWeight(Coordinate position, Direction move) {
        if (knownWalls.contains(move.getWall(position))) {
            return 0;
        }
        return super.getMoveWeight(position, move);
    }

    @Override
    public void log(ArrayList<Direction> move) {
        lastMove = move;
    }

    @Override
    public void log(Player player, int diceRoll, Coordinate originalPosition, Coordinate newPosition) {

        if (player != getPlayer() || lastMove == null) {
            return;
        }

        int newPositionIndex = -1;
        Coordinate position = originalPosition;

        //System.out.println("hi");
        for (int i = 0; i <= lastMove.size(); i++) {
            if (position.equals(newPosition)) {
                if (newPositionIndex >= 0) {
                    return;
                } else {
                    newPositionIndex = i;
                }
            }
            if (i < lastMove.size()) {
                position = lastMove.get(i).getTarget(position);
            }
        }

        if (newPositionIndex >= 0 && newPositionIndex < lastMove.size()) {
            addKnownWall(lastMove.get(newPositionIndex).getWall(newPosition));
            PlayerController controller = player.getController();
            if (controller instanceof AIPlayer) {
                ((AIPlayer) player.getController()).clearPath();
            }
        }
    }

    @Override
    public int getColor(Wall wall) {
        return knownWalls.contains(wall) ? 0 : super.getColor(wall);
    }
}
