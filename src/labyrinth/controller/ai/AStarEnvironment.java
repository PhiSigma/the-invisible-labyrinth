package labyrinth.controller.ai;

import labyrinth.Coordinate;
import labyrinth.Direction;

import java.util.List;

public interface AStarEnvironment {
    boolean isGoal(Coordinate position);

    List<Direction> getDirections(PriorityNode currentNode);

    float heuristic(Coordinate target);

    float getCost(Coordinate position, Direction move);
}
