package labyrinth.controller.ai;

import labyrinth.Coordinate;
import labyrinth.Direction;
import labyrinth.Player;
import labyrinth.Wall;

import java.util.ArrayList;

public interface AI {

    ArrayList<Direction> getPath(Coordinate position, int diceRoll);

    void log(ArrayList<Direction> move);

    void log(Player player, int diceRoll, Coordinate originalPosition, Coordinate newPosition);

    int getColor(Wall wall);
}
