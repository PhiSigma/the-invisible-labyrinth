package labyrinth.controller.ai;

import labyrinth.Coordinate;

import java.util.ArrayList;

public class PriorityNode implements Comparable<PriorityNode> {
    private Coordinate position;
    private float priority;
    private float g;
    private ArrayList<PriorityNode> parents;

    PriorityNode(Coordinate position, float priority, float g) {
        this.position = position;
        this.priority = priority;
        this.g = g;
        parents = new ArrayList<>();
    }

    Coordinate getPosition() {
        return position;
    }

    float getG() {
        return g;
    }

    void addParent(PriorityNode parent) {
        parents.add(parent);
    }

    ArrayList<PriorityNode> getParents() {
        return parents;
    }

    @Override
    public int compareTo(PriorityNode o) {
        if (o.position.equals(position)) {
            return 0;
        } else if (o.priority == priority) {
            if (o.position.getX() == position.getX()) {
                return position.getY() - o.position.getY();
            } else {
                return position.getX() - o.position.getX();
            }
        } else {
            return priority > o.priority ? 1 : -1;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (! (obj instanceof PriorityNode)) {
            return false;
        }
        PriorityNode o = (PriorityNode) obj;
        return o.position.equals(position);
    }

    @Override
    public int hashCode() {
        return position.hashCode();
    }
}
