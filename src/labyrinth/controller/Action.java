package labyrinth.controller;

public enum Action {
    START,
    LEFT,
    RIGHT,
    UP,
    DOWN,
    BACK,
    END,
    ;
}