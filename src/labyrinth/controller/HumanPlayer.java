package labyrinth.controller;

import labyrinth.Coordinate;
import labyrinth.LabyrinthApp;
import labyrinth.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static processing.core.PConstants.*;

public class HumanPlayer implements PlayerController {

    private LabyrinthApp app;

    private HashMap<Action, HashSet<Character>> keyMap = new HashMap<>() {{
        put(Action.START, new HashSet<>() {{ add('q'); add(ENTER); add(TAB); add(' ');}});
        put(Action.LEFT, new HashSet<>() {{ add('a'); }});
        put(Action.RIGHT, new HashSet<>() {{ add('d'); }});
        put(Action.UP, new HashSet<>() {{ add('w'); }});
        put(Action.DOWN, new HashSet<>() {{ add('s'); }});
        put(Action.BACK, new HashSet<>() {{ add('e'); add(BACKSPACE); add(DELETE); }});
        put(Action.END, new HashSet<>() {{ add('q'); add(ENTER); add(TAB); add(' ');}});
    }};

    public HumanPlayer(LabyrinthApp app) {
        this.app = app;
    }

    @Override
    public ArrayList<Action> askInput() {
        ArrayList<Action> actions = new ArrayList<>();

        ArrayList<Character> keyPresses = app.getKeyPresses();
        for (Character key : keyPresses) {
            for (Action action : Action.values()) {
                if (keyMap.get(action).contains(key)) {
                    actions.add(action);
                }
            }
        }

        return actions;
    }


    public void log(Player player, int diceRoll, Coordinate originalPosition, Coordinate newPosition) {

    }
}
