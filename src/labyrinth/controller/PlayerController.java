package labyrinth.controller;

import labyrinth.Coordinate;
import labyrinth.Player;

import java.util.ArrayList;

public interface PlayerController {
    ArrayList<Action> askInput();
    void log(Player player, int diceRoll, Coordinate originalPosition, Coordinate newPosition);
}
