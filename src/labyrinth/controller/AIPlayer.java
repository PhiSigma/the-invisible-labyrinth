package labyrinth.controller;

import labyrinth.*;
import labyrinth.controller.ai.*;
import labyrinth.display.LabyrinthWall;

import java.util.ArrayList;
import java.util.HashSet;

public class AIPlayer implements PlayerController {

    private Player player;
    private AI weakerAI;
    private AI strongerAI;
    private float strength;
    private ArrayList<Direction> path;

    private AIPlayer(Player player, AI weakerAI, AI strongerAI, float strength) {
        this.player = player;
        this.weakerAI = weakerAI;
        this.strongerAI = strongerAI;
        this.strength = strength;
        this.path = new ArrayList<>();
    }

    private static AI makeAI(Player player, int index) {
        switch(index) {
            case 0:
                return new RandomAI();
            case 1:
                return new WeightedRandomAI(player);
            case 2:
                return new RememberWallsAI(player);
            case 3:
                return new AStarAI(player);
            case 4:
                return new AStar4AI(player);
            case 5:
            default:
                return new BayesStarAI(player);
        }
    }

    public static AIPlayer bayes(Player player, float bayesFactor, float bayesCutoff) {
        AI weakerAI = makeAI(player, 0);
        AI strongerAI = new BayesStarAI(player, bayesFactor, bayesCutoff);
        return new AIPlayer(player, weakerAI, strongerAI, 1);
    }

    public static AIPlayer fromStrength(Player player, float strength) {
        float[] weights = new float[] {1, 1, 3, 1, 1};

        float weightSum = 0;
        for (float weight : weights) {
            weightSum += weight;
        }

        strength *= weightSum;
        weightSum = 0;
        int index = 0;

        for (int i = 0; i < weights.length; i++) {
            weightSum += weights[i];
            if (strength <= weightSum) {
                index = i;
                strength = (strength-weightSum+weights[i])/weights[i];
                break;
            }
        }

        AI weakerAI = makeAI(player, index);
        AI strongerAI = makeAI(player, index+1);
        return new AIPlayer(player, weakerAI, strongerAI, strength);
    }

    @Override
    public ArrayList<Action> askInput() {

        ArrayList<Action> actions = new ArrayList<>();
        ArrayList<Direction> moves = new ArrayList<>();

        Coordinate position = player.getPosition();
        int diceRoll = player.getDiceRoll();

        if (path.size() == 0) {
            if (Math.random() < strength) {
                path = strongerAI.getPath(position, diceRoll);
            } else {
                path = weakerAI.getPath(position, diceRoll);
            }
        }

        for (int i = 0; i < diceRoll; i++) {
            Direction move;
            if (path.size() > 0) {
                move = path.get(0);
                path.remove(0);
            } else {
                move = RandomAI.randomMove();
            }
            actions.add(Action.valueOf(move.name()));
            moves.add(move);
        }

        actions.add(Action.END);

        weakerAI.log(moves);
        strongerAI.log(moves);

        return actions;
    }

    @Override
    public void log(Player player, int diceRoll, Coordinate originalPosition, Coordinate newPosition) {
        weakerAI.log(player, diceRoll, originalPosition, newPosition);
        strongerAI.log(player, diceRoll, originalPosition, newPosition);
    }

    public void clearPath() {
        path.clear();
    }

    public HashSet<LabyrinthWall> getColoredWalls(HashSet<Wall> walls, LabyrinthApp app,
                                                  float lineWeight, float paddingX, float paddingY) {
        HashSet<LabyrinthWall> coloredWalls = new HashSet<>();
        for (Wall wall : walls) {
            coloredWalls.add(LabyrinthWall.of(wall, app, lineWeight, paddingX, paddingY,
                    strongerAI::getColor));
        }
        return coloredWalls;
    }
}
