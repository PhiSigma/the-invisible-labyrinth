package labyrinth;

import labyrinth.controller.HumanPlayer;
import labyrinth.display.menu.MainMenu;
import labyrinth.display.menu.SettingsScreen;
import labyrinth.menu.EventType;
import labyrinth.menu.KeyPress;
import labyrinth.menu.MousePress;
import labyrinth.display.Screen;
import labyrinth.display.ScreenElement;
import processing.core.PApplet;

import java.io.*;
import java.util.ArrayList;

public class LabyrinthApp extends PApplet {

    private Screen mainScreen;
    private MainMenu mainMenu;
    private SettingsScreen settingsScreen;
    private Game game;
    private Settings settings;
    private boolean inGame;
    private boolean inSettings;

    private ArrayList<Character> keyPresses = new ArrayList<>();

    public void startGame() {
        game = new Game(this, settings.getGridWidth(), settings.getGridHeight(),
                settings.getWallFactor());
        for (int i = 0; i < settings.getPlayerCount(); i++) {
            if (settings.isHuman(i)) {
                game.addPlayer(new Player(settings.getColor(i), new HumanPlayer(this)));
            } else {
                game.addPlayer(new Player(settings.getColor(i), settings.getStrength(i)));
            }
        }
        inGame = true;
        inSettings = false;
        game.start();
    }

    public void mainMenu() {
        load(mainMenu);
        inGame = false;
        inSettings = false;
        try {
            FileOutputStream fileOut = new FileOutputStream("settings.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(settings);
            out.close();
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void changeSettings() {
        load(settingsScreen);
        inGame = false;
        inSettings = true;
    }

    public Settings getSettings() {
        return settings;
    }

    @Override
    public void settings() {
        settings = new Settings();
        try {
            FileInputStream fileIn = new FileInputStream("settings.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            settings = (Settings) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException ignore) {

        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }
        size(settings.getWindowWidth(), settings.getWindowHeight());
    }

    @Override
    public void setup() {
        mainScreen = new Screen(this, 0, 0, width, height, 0);
        mainMenu = new MainMenu(this, 0, 0, width, height);
        settingsScreen = new SettingsScreen(this, 0, 0, width, height);
        mainMenu();
    }

    @Override
    public void draw() {
        if (game != null) {
            game.update();
        }
        mainScreen.draw();
    }

    @Override
    public void mousePressed() {
        mainScreen.executeEvent(new MousePress(EventType.MOUSEPRESSED, mouseButton, mouseX, mouseY));
    }

    @Override
    public void mouseReleased() {
        mainScreen.executeEvent(new MousePress(EventType.MOUSERELEASED, mouseButton, mouseX, mouseY));
    }

    @Override
    public void mouseClicked() {
        mainScreen.executeEvent(new MousePress(EventType.MOUSECLICKED, mouseButton, mouseX, mouseY));
    }

    @Override
    public void mouseDragged() {
        mainScreen.executeEvent(new MousePress(EventType.MOUSEDRAGGED, mouseButton, mouseX, mouseY));
    }

    @Override
    public void keyReleased() {
        keyPresses.add(key);
        if (inGame && (key == ENTER || key == 'm')) {
            mainMenu();
        }
        mainScreen.executeEvent(new KeyPress(EventType.KEYRELEASED, key, keyCode));
    }

    @Override
    public void keyPressed() {
        if (key == CODED) {
            switch(keyCode) {
                case LEFT:
                    scaleWindow(1/settings.getScaleIncrement(), 1);
                    break;
                case RIGHT:
                    scaleWindow(settings.getScaleIncrement(), 1);
                    break;
                case UP:
                    scaleWindow(1, 1/settings.getScaleIncrement());
                    break;
                case DOWN:
                    scaleWindow(1, settings.getScaleIncrement());
            }
        }
        mainScreen.executeEvent(new KeyPress(EventType.KEYPRESSED, key, keyCode));
    }

    void load(ScreenElement element) {
        mainScreen.loadElement(element, true);
    }

    public ArrayList<Character> getKeyPresses() {
        final ArrayList<Character> oldPresses = keyPresses;
        keyPresses = new ArrayList<>();
        return oldPresses;
    }

    private void scaleWindow(float xScale, float yScale) {
        int oldWidth = width;
        int oldHeight = height;
        int newWidth = (int) (width*xScale);
        int newHeight = (int) (height*yScale);
        surface.setSize(newWidth, newHeight);
        settings.setWindowWidth(width);
        settings.setWindowHeight(height);
        mainScreen.resizeScreen((float) newWidth/oldWidth, (float) newHeight/oldHeight);
        if (!inSettings) {
            settingsScreen.resizeScreen((float) newWidth/oldWidth, (float) newHeight/oldHeight);
        }
        if (inSettings || inGame) {
            mainMenu.resizeScreen((float) newWidth/oldWidth, (float) newHeight/oldHeight);
        }
    }

    public static void main(String[] args) {
	    PApplet.main(LabyrinthApp.class.getCanonicalName());
    }
}
