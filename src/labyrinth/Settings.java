package labyrinth;

import java.io.Serializable;

public class Settings implements Serializable {

    private int windowWidth = 800;
    private int windowHeight = 600;
    private float scaleIncrement = 1.04f;

    private float defaultPlayerSize = 5f;
    private float playerSize = 5f;

    private float lineWeightWall = 0.05f;

    private int gridWidth = 5;
    private int gridHeight = 4;

    private float wallFactor = 0.5f;
    private int dieSize = 6;

    private boolean previewMoves = false;
    private boolean showAIColors = true;

    private int playerCount = 2;
    private int[] colors = {-16756481, -16726016, -14336, -65536, -65336, -10197916};
    private boolean[] humans = {true, true, true, true, true, true};
    private float[] aiStrengths = {0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f};

    int getWindowWidth() {
        return windowWidth;
    }

    void setWindowWidth(int windowWidth) {
        this.windowWidth = windowWidth;
    }

    int getWindowHeight() {
        return windowHeight;
    }

    void setWindowHeight(int windowHeight) {
        this.windowHeight = windowHeight;
    }

    float getScaleIncrement() {
        return scaleIncrement;
    }

    public float getPlayerSize() {
        return playerSize;
    }

    public void resetPlayerSize() {
        playerSize = defaultPlayerSize;
    }

    public void decreasePlayerSize() {
        playerSize *= 0.98f;
    }

    float getLineWeightWall() {
        return lineWeightWall;
    }

    public boolean everyoneIsAI() {
        for (int i = 0; i < playerCount; i++) {
            if (humans[i]) {
                return false;
            }
        }
        return true;
    }

    public boolean isShowingAIColors() {
        return showAIColors;
    }

    boolean isReallyShowingAIColors() {
        return showAIColors && everyoneIsAI();
    }

    public void setShowAIColors(boolean showAIColors) {
        this.showAIColors = showAIColors;
    }

    public int getGridWidth() {
        return gridWidth;
    }

    public void setGridWidth(int gridWidth) {
        this.gridWidth = gridWidth;
    }

    public int getGridHeight() {
        return gridHeight;
    }

    public void setGridHeight(int gridHeight) {
        this.gridHeight = gridHeight;
    }

    public float getWallFactor() {
        return wallFactor;
    }

    public void setWallFactor(float wallFactor) {
        this.wallFactor = wallFactor;
    }

    int getDieSize() {
        return dieSize;
    }

    public boolean isPreviewingMoves() {
        return previewMoves;
    }

    public void setPreviewMoves(boolean previewMoves) {
        this.previewMoves = previewMoves;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public void decreasePlayerCount() {
        if (playerCount > 1) {
            playerCount--;
        }
    }

    public void increasePlayerCount() {
        if (playerCount < 6) {
            playerCount++;
        }
    }

    public int getColor(int index) {
        return colors[index];
    }

    public void setColor(int index, int color) {
        colors[index] = color;
    }

    public boolean isHuman(int index) {
        return humans[index];
    }

    public void setHuman(int index, boolean isHuman) {
        humans[index] = isHuman;
    }

    public float getStrength(int index) {
        return aiStrengths[index];
    }

    public void setStrength(int index, float strength) {
        aiStrengths[index] = strength;
    }
}