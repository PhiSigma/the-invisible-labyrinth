package labyrinth.display;

import labyrinth.LabyrinthApp;
import labyrinth.Wall;

import java.util.function.Function;

public class LabyrinthWall extends ScreenElement {
    private float lineWeight;
    private Function<Wall, Integer> colorFunction;
    private Wall wall;

    static LabyrinthWall of(Wall wall, LabyrinthApp app, float lineWeight,
                            float paddingX, float paddingY) {
        return of(wall, app, lineWeight, paddingX, paddingY, w -> 0);
    }

    public static LabyrinthWall of(Wall wall, LabyrinthApp app, float lineWeight,
                                   float paddingX, float paddingY,
                                   Function<Wall, Integer> colorFunction) {
        int leftX = wall.getX();
        int rightX = wall.getX();
        int upY = wall.getY();
        int downY = wall.getY();

        if (wall.isHorizontal()) {
            rightX++;
        } else {
            downY++;
        }

        return new LabyrinthWall(app, leftX+1+paddingX, upY+paddingY,
                rightX+1+paddingX, downY+paddingY, lineWeight, wall, colorFunction);
    }

    private LabyrinthWall(LabyrinthApp app, float leftX, float upY, float rightX, float downY,
                         float lineWeight, Wall wall, Function<Wall, Integer> colorFunction) {
        super(app, leftX*10, upY*10, rightX*10, downY*10, 1, ScaleMode.MIN);
        this.lineWeight = lineWeight;
        this.colorFunction = colorFunction;
        this.wall = wall;
    }

    @Override
    public void draw() {
        LabyrinthApp app = getApp();
        app.stroke(colorFunction.apply(wall));
        app.strokeWeight(lineWeight*10);
        app.line(getLeftX(), getUpY(), getRightX(), getDownY());
    }
}
