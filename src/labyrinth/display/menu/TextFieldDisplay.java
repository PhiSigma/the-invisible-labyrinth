package labyrinth.display.menu;

import labyrinth.LabyrinthApp;

public class TextFieldDisplay extends TextDisplay {
    private int color;
    private float strokeWeightFactor;

    public TextFieldDisplay(LabyrinthApp app, float leftX, float upY, float rightX, float downY,
                            String defaultText) {
        super(app, leftX, upY, rightX, downY, defaultText);
        resetColor();
        resetStrokeWeightFactor();
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void resetColor() {
        color = getApp().color(255);
    }

    public void setStrokeWeightFactor(float factor) {
        strokeWeightFactor = factor;
    }

    public void resetStrokeWeightFactor() {
        strokeWeightFactor = 0.01f;
    }

    @Override
    public void draw() {
        final float leftX = getLeftX(), upY = getUpY(), rightX = getRightX(), downY = getDownY();

        final float width = rightX - leftX;
        final float height = downY - upY;

        LabyrinthApp app = getApp();

        app.fill(color);
        app.stroke(0);
        app.strokeWeight(Math.min(width, height)*strokeWeightFactor);

        app.rect(leftX, upY, width, height);

        super.draw();
    }
}
