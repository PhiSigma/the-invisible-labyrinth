package labyrinth.display.menu;

import labyrinth.LabyrinthApp;
import labyrinth.menu.MousePress;

import static processing.core.PApplet.*;

public class SVTriangleDisplay extends HuePickerDisplay {
    private float hue;
    private float saturation;

    public SVTriangleDisplay(LabyrinthApp app, float centerX, float centerY, float radius, float radiusFactor) {
        super(app, centerX, centerY, radius, radiusFactor);
    }

    public void setHue(float hue) {
        this.hue = hue;
    }

    public void setSaturation(float saturation) {
        this.saturation = saturation;
    }

    private float getLocalX(float x, float y) {
        final float alpha = getAlpha();
        x -= getCenterX();
        y -= getCenterY();
        x = cos(alpha)*x - sin(alpha)*y;
        x /= (getRadius()*(1-getThicknessFactor())*sqrt(3)/2);
        return x;
    }

    private float getLocalY(float x, float y) {
        final float alpha = getAlpha();
        final float r = getRadius()*(1-getThicknessFactor());
        x -= getCenterX();
        y -= getCenterY();
        y = sin(alpha)*x + cos(alpha)*y;
        y -= r/2;
        y /= (-r*3/2);
        return y;
    }

    private float xyToSaturation(float x, float y) {
        if (y < 1) {
            return Math.max(0, Math.min(100, 50 + 50*x/(1-y)));
        } else {
            return 0;
        }
    }

    private float yToValue(float y) {
        return Math.max(0, Math.min(100, 100*(1-y)));
    }

    private float getLocalX() {
        return getValue()/100*(saturation-50)/50;
    }

    private float getLocalY() {
        return 1-getValue()/100;
    }

    private float getAlpha() {
        return radians((-hue+30) % 360);
    }

    private float getGlobalX() {
        final float alpha = getAlpha();
        final float r = getRadius()*(1-getThicknessFactor());
        float x = getLocalX();
        float y = getLocalY();
        y *= (-r*3/2);
        y += r/2;
        x *= (getRadius()*(1-getThicknessFactor())*sqrt(3)/2);
        x = cos(alpha)*x + sin(alpha)*y;
        x += getCenterX();
        return x;
    }

    private float getGlobalY() {
        final float alpha = getAlpha();
        final float r = getRadius()*(1-getThicknessFactor());
        float x = getLocalX();
        float y = getLocalY();
        x *= (getRadius()*(1-getThicknessFactor())*sqrt(3)/2);
        y *= (-r*3/2);
        y += r/2;
        y = -sin(alpha)*x + cos(alpha)*y;
        y += getCenterY();
        return y;
    }

    private boolean isInside(float x, float y) {
        return 0 <= y && y <= 1-Math.abs(x);
    }

    private boolean isFarInside(float x, float y) {
        return 0.005f <= y && y <= 1-Math.abs(x)-0.015f;
    }

    @Override
    public void draw() {
        final float leftX = getLeftX(), upY = getUpY(), rightX = getRightX(), downY = getDownY();
        final float centerX = getCenterX(), centerY = getCenterY();
        final float radius = getRadius();
        final float smallRadius = radius*(1-getThicknessFactor());
        final float width = rightX - leftX;

        LabyrinthApp app = getApp();

        final int n = 300;
        final float padding = width*getThicknessFactor()/2;
        final float strokeWeight = (width-2*padding)/n*2.5f;

        app.strokeWeight(strokeWeight);
        app.colorMode(HSB, 360, 100, 100);

        for (int x = 0; x <= n; x++) {
            for (int y = 0; y <= n; y++) {
                final float globalX = getLeftX() + padding + x*(width-2*padding)/n;
                final float globalY = getUpY() + padding + y*(width-2*padding)/n;
                final float localX = getLocalX(globalX, globalY), localY = getLocalY(globalX, globalY);
                if (isFarInside(localX, localY)) {
                    app.stroke(hue, xyToSaturation(localX, localY), yToValue(localY));
                    app.point(globalX, globalY);
                }
            }
        }

        final float c = cos(getAlpha());
        final float s = sin(getAlpha());

        app.stroke(0);
        app.strokeWeight(radius*0.02f);
        app.triangle(centerX - s*smallRadius, centerY - c*smallRadius,
                centerX - c*smallRadius*sqrt(3)/2 + s*smallRadius/2,
                centerY + c*smallRadius/2 + s*smallRadius*sqrt(3)/2,
                centerX + c*smallRadius*sqrt(3)/2 + s*smallRadius/2,
                centerY + c*smallRadius/2 - s*smallRadius*sqrt(3)/2);
        app.colorMode(RGB, 255, 255, 255);

        app.strokeWeight(radius*0.01f);
        app.fill(255);
        app.ellipseMode(CENTER);
        app.ellipse(getGlobalX(), getGlobalY(), radius*0.06f, radius*0.06f);
    }

    @Override
    public boolean isInside(MousePress mousePress) {
        final float x = mousePress.getMouseX(), y = mousePress.getMouseY();
        return isInside(getLocalX(x, y), getLocalY(x, y));
    }

    @Override
    public float calculateValue(MousePress mousePress) {
        final float x = mousePress.getMouseX(), y = mousePress.getMouseY();
        return yToValue(getLocalY(x, y));
    }

    public float calculateSaturation(MousePress mousePress) {
        final float x = mousePress.getMouseX(), y = mousePress.getMouseY();
        return xyToSaturation(getLocalX(x, y), getLocalY(x, y));
    }
}
