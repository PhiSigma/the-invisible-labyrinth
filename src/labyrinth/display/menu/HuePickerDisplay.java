package labyrinth.display.menu;

import labyrinth.LabyrinthApp;
import labyrinth.menu.MousePress;

import static processing.core.PApplet.*;
import static processing.core.PConstants.HSB;
import static processing.core.PConstants.PI;

public class HuePickerDisplay extends SliderDisplay {

    public HuePickerDisplay(LabyrinthApp app, float centerX, float centerY, float radius, float radiusFactor) {
        super(app, centerX-radius, centerY-radius, centerX+radius,
                centerY+radius, radiusFactor);
    }

    @Override
    public void draw() {
        final float centerX = getCenterX(), centerY = getCenterY();
        final float radius = getRadius();
        final float smallRadius = radius*(1-getThicknessFactor());

        final int linesPerDegree = 1;

        LabyrinthApp app = getApp();

        app.noFill();
        final float strokeWeight = 1.8f * 2*PI*radius / (360*linesPerDegree);
        app.strokeWeight(strokeWeight);
        app.colorMode(HSB, 360, 100, 100);
        app.ellipseMode(CENTER);

        for (int i = 0; i < 360*linesPerDegree; i++) {
            float degree = (float) i/linesPerDegree;
            drawLine(app, centerX, centerY, radius-strokeWeight/2,
                    smallRadius+strokeWeight/2, degree, false);
        }

        drawLine(app, centerX, centerY, radius-strokeWeight/2,
                smallRadius+strokeWeight/2, getValue(), true);

        app.stroke(0);
        app.strokeWeight(radius*0.02f);
        app.ellipse(centerX, centerY, radius*2, radius*2);
        app.ellipse(centerX, centerY, smallRadius*2, smallRadius*2);
        app.colorMode(RGB, 255, 255, 255);
    }

    private void drawLine(LabyrinthApp app, float centerX, float centerY, float radius, float smallRadius,
                          float degree, boolean isBlack) {
        float angle = radians(degree);
        float x = cos(angle);
        float y = sin(angle);
        app.stroke(degree, 100, isBlack ? 0 : 100);
        app.line(centerX + x * radius, centerY + y * radius,
                centerX + x * smallRadius, centerY + y * smallRadius);
    }

    float getCenterX() {
        return (getLeftX()+getRightX())/2;
    }

    float getCenterY() {
        return (getUpY()+getDownY())/2;
    }

    float getRadius() {
        return getCenterX()-getLeftX();
    }

    @Override
    public float calculateValue(MousePress mousePress) {
        final float centerX = getCenterX();
        final float centerY = getCenterY();
        final float x = mousePress.getMouseX() - centerX;
        final float y = mousePress.getMouseY() - centerY;
        if (x == 0 && y == 0) {
            return 0;
        }
        return degrees(((float) Math.atan2(y, x) + TAU) % TAU);
    }

    @Override
    public boolean isInside(MousePress mousePress) {
        final float centerX = getCenterX(), centerY = getCenterY();
        final float radius = getRadius();
        final float smallRadius = radius*(1-getThicknessFactor());

        final float x = mousePress.getMouseX() - centerX;
        final float y = mousePress.getMouseY() - centerY;
        final float r2 = x*x + y*y;

        return r2 <= radius*radius && r2 >= smallRadius*smallRadius;
    }
}
