package labyrinth.display.menu;

import labyrinth.LabyrinthApp;

public class ButtonDisplay extends TextDisplay {

    public ButtonDisplay(LabyrinthApp app, float leftX, float upY, float rightX, float downY, String text) {
        super(app, leftX, upY, rightX, downY, text);
    }

    @Override
    public void draw() {
        final float leftX = getLeftX(), upY = getUpY(), rightX = getRightX(), downY = getDownY();

        final float width = rightX - leftX;
        final float height = downY - upY;
        final float rounding = Math.min(width, height)*0.2f;

        LabyrinthApp app = getApp();

        app.fill(255);
        app.stroke(0);
        app.strokeWeight(Math.min(width, height)*0.05f);

        app.rect(leftX, upY, width, height, rounding);

        super.draw();
    }
}
