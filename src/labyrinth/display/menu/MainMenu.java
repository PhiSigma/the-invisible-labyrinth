package labyrinth.display.menu;

import labyrinth.LabyrinthApp;
import labyrinth.display.FlatBackground;
import labyrinth.display.Screen;
import labyrinth.display.menu.TextDisplay;
import labyrinth.menu.Button;

public class MainMenu extends Screen {
    public MainMenu(LabyrinthApp app, float leftX, float upY, float rightX, float downY) {
        super(app, leftX, upY, rightX, downY, 0);

        final int buttonPadding = 200;
        final int buttonSpacing = 100;
        final int buttonHeight = 200;
        int buttonStart = 1000;

        loadElement(new FlatBackground(app, 0, 0, 2000, 2000, app.color(255)));
        loadElement(new TextDisplay(app, 0, 400, 2000, 700,
                "Invisible Labyrinth"));

        loadElement(new Button(app, buttonPadding, buttonStart, 2000-buttonPadding,
                buttonStart+buttonHeight, "Start Game", app::startGame));
        buttonStart += buttonHeight + buttonSpacing;
        loadElement(new Button(app, buttonPadding, buttonStart, 2000-buttonPadding,
                buttonStart+buttonHeight, "Change Settings", app::changeSettings));
        buttonStart += buttonHeight + buttonSpacing;
        loadElement(new Button(app, buttonPadding, buttonStart, 2000-buttonPadding,
                buttonStart+buttonHeight, "Quit", app::exit));
    }
}
