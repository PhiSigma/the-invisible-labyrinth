package labyrinth.display.menu;

import labyrinth.LabyrinthApp;
import labyrinth.display.ScaleMode;
import labyrinth.display.ScreenElement;
import labyrinth.menu.MousePress;

import static processing.core.PApplet.sqrt;

public class SliderDisplay extends ScreenElement {
    private float value;
    private float thicknessFactor;

    public SliderDisplay(LabyrinthApp app, float leftX, float upY, float rightX, float rightY,
                         float thicknessFactor) {
        super(app, leftX, upY, rightX, rightY, 15, ScaleMode.MIN);
        this.value = 0;
        this.thicknessFactor = thicknessFactor;
    }

    float getThicknessFactor() {
        return thicknessFactor;
    }

    float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public void draw() {
        final float leftX = getLeftX(), upY = getUpY(), rightX = getRightX(), downY = getDownY();

        final float width = rightX - leftX;
        final float height = downY - upY;

        LabyrinthApp app = getApp();

        app.fill(255);
        app.stroke(0);
        app.strokeWeight(Math.min(width, height)*0.05f);

        app.rect(leftX, upY, width, height*thicknessFactor);

        final float sliderPosition = leftX + value*width;
        final float triangleHeight = height*(1-thicknessFactor);
        final float triangleWidth = triangleHeight/sqrt(3);

        app.line(sliderPosition, upY, sliderPosition, downY-triangleHeight);
        app.triangle(sliderPosition, downY-triangleHeight,
                sliderPosition-triangleWidth, downY,
                sliderPosition+triangleWidth, downY);
    }

    private boolean isInRectangle(float x, float y) {
        final float leftX = getLeftX(), upY = getUpY(), rightX = getRightX(),
                downY = upY + (getDownY()-upY)*thicknessFactor;
        return x >= leftX && x <= rightX && y >= upY && y <= downY;
    }

    private boolean isInTriangle(float x, float y) {
        final float leftX = getLeftX(), upY = getUpY(), rightX = getRightX(), downY = getDownY();

        final float width = rightX - leftX;
        final float height = downY - upY;

        final float sliderPosition = leftX + value*width;
        final float triangleHeight = height*(1-thicknessFactor);

        x -= sliderPosition;
        y -= downY;

        return -y > 0 && -y < triangleHeight - sqrt(3)*Math.abs(x);
    }

    public boolean isInside(MousePress mousePress) {
        final float x = mousePress.getMouseX(), y = mousePress.getMouseY();
        return isInRectangle(x, y) || isInTriangle(x, y);
    }

    public float calculateValue(MousePress mousePress) {
        final float leftX = getLeftX(), rightX = getRightX();
        final float x = mousePress.getMouseX();
        if (x < leftX) {
            return 0;
        }
        if (x > rightX) {
            return 1;
        }
        return (x-leftX)/(rightX-leftX);
    }
}