package labyrinth.display.menu;

import labyrinth.LabyrinthApp;
import labyrinth.display.ScaleMode;
import labyrinth.display.ScreenElement;

public class TextDisplay extends ScreenElement {
    private String text;
    private int textColor;

    public TextDisplay(LabyrinthApp app, float leftX, float upY, float rightX, float downY, String text) {
        super(app, leftX, upY, rightX, downY, 20, ScaleMode.MIN);
        this.text = text;
        resetTextColor();
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public void resetTextColor() {
        textColor = getApp().color(0);
    }

    @Override
    public void draw() {
        final float leftX = getLeftX(), upY = getUpY(), rightX = getRightX(), downY = getDownY();

        final float width = rightX - leftX;
        final float height = downY - upY;

        LabyrinthApp app = getApp();

        app.textAlign(app.CENTER, app.CENTER);
        app.textSize(height*0.7f);
        app.fill(textColor);

        app.text(text, leftX, upY, width, height);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
