package labyrinth.display.menu;

import labyrinth.LabyrinthApp;
import labyrinth.Settings;
import labyrinth.display.FlatBackground;
import labyrinth.display.Screen;
import labyrinth.display.menu.TextDisplay;
import labyrinth.menu.*;

import java.util.ArrayList;

public class SettingsScreen extends Screen {
    private ArrayList<PlayerSettings> playerSettings;
    private boolean everyoneIsAI;
    private Checkbox movePreview;
    private TextDisplay movePreviewExplanation;
    private Checkbox aiColors;
    private TextDisplay aiColorsExplanation;
    private Button plusButton;
    private Button minusButton;
    private IntegerField widthField;
    private IntegerField heightField;

    public SettingsScreen(LabyrinthApp app, float leftX, float upY, float rightX, float downY) {
        super(app, leftX, upY, rightX, downY, 0);

        final Settings settings = app.getSettings();

        loadElement(new FlatBackground(app, 0, 0, 2000, 1500, app.color(255)));
        loadElement(new TextDisplay(app, 0, 0, 2000, 200,
                "Settings"));

        int playerCount = settings.getPlayerCount();

        playerSettings = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            playerSettings.add(new PlayerSettings(app, (i%2)*1000, 650+250*(i/2),
                    (i%2 + 1)*1000, 650+250*(i/2 + 1), i));
        }
        for (int i = 0; i < playerCount; i++) {
            loadElement(playerSettings.get(i));
        }

        loadElement(new TextDisplay(app, 20, 260, 480, 340, "Labyrinth Size"));
        widthField = new IntegerField(app, 20, 350, 240, 450, settings::setGridWidth,
                settings.getGridWidth(), 3, 100);
        loadElement(widthField);
        heightField = new IntegerField(app, 260, 350, 480, 450, settings::setGridHeight,
                settings.getGridHeight(), 2, 80);
        loadElement(heightField);

        loadElement(new TextDisplay(app, 500, 280, 1250, 320,
                "Wall Factor (the higher, the more walls)"));
        loadElement(new Slider(app, 500, 350, 1250, 450, 0.5f,
                settings::setWallFactor, settings.getWallFactor()));

        movePreviewExplanation = new TextDisplay(app, 1250, 380, 2000, 420,
                "Please look away during turns of other players!");
        movePreview = new Checkbox(app, 1250, 260, 2000, 340,
                "Preview Moves", this::setPreviewMoves, settings.isPreviewingMoves());

        aiColorsExplanation = new TextDisplay(app, 1250, 380, 2000, 420,
                "See where the AI thinks the walls are.");
        aiColors = new Checkbox(app, 1250, 260, 2000, 340,
                "AI Color Map", settings::setShowAIColors,
                settings.isShowingAIColors());

        everyoneIsAI = settings.everyoneIsAI();
        if (everyoneIsAI) {
            loadElement(aiColors);
            loadElement(aiColorsExplanation);
        } else {
            loadElement(movePreview);
            if (settings.isPreviewingMoves()) {
                loadElement(movePreviewExplanation);
            }
        }

        loadElement(new Button(app, 20, 1420, 180, 1480,
                "PvP", this::presetPvP));
        loadElement(new Button(app, 220, 1420, 380, 1480,
                "PvE", this::presetPvE));
        loadElement(new Button(app, 420, 1420, 580, 1480,
                "EvE", this::presetEvE));
        loadElement(new Button(app, 620, 1420, 1080, 1480,
                "Match AI Strength", this::matchAI));
        loadElement(new Button(app, 1620, 1420, 1980, 1480,
                "Back", app::mainMenu));

        plusButton = new Button(app, 20, 550, 120, 650, "+", this::addPlayer);
        minusButton = new Button(app, 120, 550, 220, 650, "-", this::removePlayer);
        if (playerCount < 6) {
            loadElement(plusButton);
        }
        if (playerCount > 1) {
            loadElement(minusButton);
        }
    }

    @Override
    public void executeEvent(InteractiveEvent event) {
        super.executeEvent(event);

        final Settings settings = getApp().getSettings();
        if (everyoneIsAI != settings.everyoneIsAI()) {
            everyoneIsAI = settings.everyoneIsAI();
            if (everyoneIsAI) {
                loadElement(aiColors);
                loadElement(aiColorsExplanation);
                removeElement(movePreview);
                removeElement(movePreviewExplanation);
            } else {
                removeElement(aiColors);
                removeElement(aiColorsExplanation);
                loadElement(movePreview);
                if (settings.isPreviewingMoves()) {
                    loadElement(movePreviewExplanation);
                }
            }
        }
    }

    private void setPreviewMoves(boolean previewMoves) {
        getApp().getSettings().setPreviewMoves(previewMoves);
        if (previewMoves) {
            loadElement(movePreviewExplanation);
        } else {
            removeElement(movePreviewExplanation);
        }
    }

    private void addPlayer() {
        final Settings settings = getApp().getSettings();
        final int oldPlayerCount = settings.getPlayerCount();
        settings.increasePlayerCount();
        final int playerCount = settings.getPlayerCount();
        if (playerCount == 6 && oldPlayerCount < 6) {
            removeElement(plusButton);
        }
        if (playerCount > 1 && oldPlayerCount == 1) {
            loadElement(minusButton);
        }
        if (playerCount > oldPlayerCount) {
            loadElement(playerSettings.get(playerCount-1));
        }
    }

    private void removePlayer() {
        final Settings settings = getApp().getSettings();
        final int oldPlayerCount = settings.getPlayerCount();
        settings.decreasePlayerCount();
        final int playerCount = settings.getPlayerCount();
        if (playerCount == 1 && oldPlayerCount > 1) {
            removeElement(minusButton);
        }
        if (playerCount < 6 && oldPlayerCount == 6) {
            loadElement(plusButton);
        }
        if (playerCount < oldPlayerCount) {
            removeElement(playerSettings.get(playerCount));
        }
    }

    private void setPlayerCount(int count) {
        if (count < 1 || count > 6) {
            return;
        }
        final Settings settings = getApp().getSettings();
        while (settings.getPlayerCount() < count) {
            addPlayer();
        }
        while (settings.getPlayerCount() > count) {
            removePlayer();
        }
    }

    private void setGridWidth(int i) {
        getApp().getSettings().setGridWidth(i);
        widthField.setValue(i);
    }

    private void setGridHeight(int i) {
        getApp().getSettings().setGridHeight(i);
        heightField.setValue(i);
    }

    private void presetPvP() {
        setPreviewMoves(false);
        movePreview.setValue(false);
        setPlayerCount(2);
        for (int i = 0; i < 6; i++) {
            playerSettings.get(i).externalSetAI(false);
        }
        setGridWidth(5);
        setGridHeight(4);
    }

    private void presetPvE() {
        setPreviewMoves(true);
        movePreview.setValue(true);
        setPlayerCount(4);
        playerSettings.get(0).externalSetAI(false);
        for (int i = 1; i < 6; i++) {
            playerSettings.get(i).externalSetAI(true);
            playerSettings.get(i).externalSetStrength(0.5f);
        }
        setGridWidth(5);
        setGridHeight(4);
    }

    private void presetEvE() {
        setPlayerCount(4);
        for (int i = 0; i < 6; i++) {
            playerSettings.get(i).externalSetAI(true);
        }
        setGridWidth(50);
        setGridHeight(40);
    }

    private void matchAI() {
        final Settings settings = getApp().getSettings();

        float avgStrength = 0;
        int aiCount = 0;
        for (int i = 0; i < 6; i++) {
            if (! settings.isHuman(i)) {
                aiCount++;
                avgStrength += settings.getStrength(i);
            }
        }
        if (aiCount == 0) {
            return;
        }
        avgStrength /= aiCount;

        float max = 0;
        int argmax = 0;
        for (int i = 0; i < 6; i++) {
            if (! settings.isHuman(i)) {
                float distance = Math.abs(settings.getStrength(i) - avgStrength);
                if (distance >= max) {
                    max = distance;
                    argmax = i;
                }
            }
        }

        float strength = settings.getStrength(argmax);

        for (int i = 0; i < 6; i++) {
            playerSettings.get(i).externalSetStrength(strength);
        }
    }
}
