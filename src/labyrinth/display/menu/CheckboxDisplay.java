package labyrinth.display.menu;

import labyrinth.LabyrinthApp;
import labyrinth.display.ScaleMode;
import labyrinth.display.ScreenElement;

public class CheckboxDisplay extends ScreenElement {
    private String text;
    private boolean value;

    public CheckboxDisplay(LabyrinthApp app, float leftX, float upY, float rightX, float downY, String text) {
        super(app, leftX, upY, rightX, downY, 20, ScaleMode.MIN);
        this.text = text;
        this.value = false;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    @Override
    public void draw() {
        final float leftX = getLeftX(), upY = getUpY(), rightX = getRightX(), downY = getDownY();

        final float width = rightX - leftX;
        final float height = downY - upY;
        final float padding = height*0.2f;

        LabyrinthApp app = getApp();

        app.fill(255);
        app.stroke(0);
        app.strokeWeight(height*0.05f);

        app.rect(leftX+padding, upY+padding, height-2*padding, height-2*padding);

        if (value) {
            app.strokeWeight(height*0.2f);
            app.line(leftX+padding, upY+height/2, leftX+height/2, downY-padding);
            app.line(leftX+height-padding, upY+padding, leftX+height/2, downY-padding);
        }

        app.textAlign(app.LEFT, app.CENTER);
        app.textSize(height*0.7f);
        app.fill(0);

        app.text(text, leftX+height, upY, width-height, height);
    }
}