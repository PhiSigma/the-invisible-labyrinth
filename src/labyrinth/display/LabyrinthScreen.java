package labyrinth.display;

import labyrinth.*;
import labyrinth.controller.AIPlayer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class LabyrinthScreen extends Screen {
    private float paddingX;
    private float paddingY;
    private float lineWeightWall;
    private FlatBackground background;
    private LabyrinthBorder border;
    private LabyrinthGrid grid;
    private HashSet<LabyrinthWall> walls;
    private ArrayList<PlayerDisplay> players;
    private ArrayList<MoveArrow> previews;

    public LabyrinthScreen(LabyrinthApp app, float leftX, float upY, float rightX, float downY, int z,
                           int gridWidth, int gridHeight, HashSet<Wall> walls,
                           float lineWeightWall, float lineWeightGrid, float paddingX, float paddingY) {
        super(app, leftX, upY, rightX, downY, z);

        this.paddingX = paddingX;
        this.paddingY = paddingY;
        this.lineWeightWall = lineWeightWall;
        border = new LabyrinthBorder(app, gridWidth, gridHeight, lineWeightWall, paddingX, paddingY);
        background = new FlatBackground(app, border.getLeftX(), border.getUpY(),
                border.getRightX(), border.getDownY(), app.color(255));
        grid = new LabyrinthGrid(app, gridWidth, gridHeight, lineWeightGrid, paddingX, paddingY);
        this.walls = new HashSet<>();
        for (Wall wall : walls) {
            this.walls.add(LabyrinthWall.of(wall, app, lineWeightWall, paddingX, paddingY));
        }
        players = new ArrayList<>();
        previews = new ArrayList<>();
        app.getSettings().resetPlayerSize();

        super.loadElement(background);
        super.loadElement(border);
        super.loadElement(grid);
    }

    public void showWalls() {
        for (LabyrinthWall wall : walls) {
            super.loadElement(wall);
        }
    }

    public void makeColoredWalls(AIPlayer controller, HashSet<Wall> walls) {
        HashSet<LabyrinthWall> coloredWalls = controller.getColoredWalls(
                walls, getApp(), lineWeightWall, paddingX, paddingY);
        for (LabyrinthWall wall : coloredWalls) {
            super.loadElement(wall);
        }
    }

    public void reset() {
        super.loadElement(background, true);
        super.loadElement(border);
        super.loadElement(grid);
        for (PlayerDisplay player : players) {
            super.loadElement(player);
        }
    }

    private void setGhostVisible(Player player, boolean isVisible) {
        for (PlayerDisplay other : players) {
            if (other instanceof PlayerGhost && other.getPlayer() == player) {
                ((PlayerGhost) other).setVisible(isVisible);
            }
        }
    }

    public void showGhost(Player player) {
        setGhostVisible(player, true);

        List<PlayerDisplay> displays = players.stream()
                .filter(p -> p.getPlayer() == player)
                .collect(Collectors.toList());
        if (displays.size() >= 2) {
            displays.get(0).swapShift(displays.get(1));
        }
    }

    public void hideGhost(Player player) {
        setGhostVisible(player, false);
    }

    public void addPlayer(Player player) {
        PlayerDisplay display = new PlayerDisplay(getApp(), grid.getLeftX(), grid.getUpY(), grid.getRightX(),
                grid.getDownY(), player, paddingX+1, paddingY);
        PlayerDisplay ghost = display.makeGhost();

        players.add(display);
        players.add(ghost);
        super.loadElement(display);
        super.loadElement(ghost);

        shiftPlayers();
    }

    public void showTurn(Player player, boolean setColor) {
        if (setColor) {
            background.setColor(player.getColor());
        }
        while (previews.size() > 0) {
            removePreview(0);
        }
    }

    public void previewMove(Player player, Coordinate position, Direction move) {
        MoveArrow moveArrow = MoveArrow.of(position, move, getApp(),
                paddingX, paddingY, player.getColor());
        previews.add(moveArrow);
        super.loadElement(moveArrow);
    }

    public void removePreview(int size) {
        if (previews.size() > size) {
            MoveArrow last = previews.get(previews.size()-1);
            previews.remove(last);
            super.removeElement(last);
        }
    }

    private void shiftPlayers() {
        float playerSize = getApp().getSettings().getPlayerSize();
        for (PlayerDisplay player : players) {
            int tries = 0;
            while (minDistance(player) < playerSize*playerSize) {
                tries++;
                shift(player);
                if (tries > 1000) {
                    getApp().getSettings().decreasePlayerSize();
                    playerSize = getApp().getSettings().getPlayerSize();
                }
            }
        }
    }

    private void shift(PlayerDisplay player) {
        LabyrinthApp app = getApp();
        player.resetShift();
        float maxShift = 0.5f-getApp().getSettings().getPlayerSize()*0.06f;
        float shiftX = app.random(-maxShift, maxShift);
        float shiftY = app.random(-maxShift, maxShift);
        player.shiftX(shiftX);
        player.shiftY(shiftY);
    }

    private float minDistance(PlayerDisplay player) {
        float minDistance = 100;
        for (PlayerDisplay other : players) {
            if (other == player) {
                continue;
            }
            float xDistance = other.getX()-player.getX();
            float yDistance = other.getY()-player.getY();
            float distance = xDistance*xDistance + yDistance*yDistance;
            if (distance < minDistance) {
                minDistance = distance;
            }
        }
        return minDistance;
    }
}
