package labyrinth.display;

import labyrinth.LabyrinthApp;

public abstract class ScreenElement implements Comparable<ScreenElement> {
    private LabyrinthApp app;
    private float leftX;
    private float upY;
    private float rightX;
    private float downY;
    private int z;
    private ScaleMode scaleMode;

    public ScreenElement(LabyrinthApp app, float leftX, float upY, float rightX, float downY,
                         int z, ScaleMode scaleMode) {
        this.app = app;
        this.leftX = leftX;
        this.upY = upY;
        this.rightX = rightX;
        this.downY = downY;
        this.z = z;
        this.scaleMode = scaleMode;
    }

    public ScreenElement(ScreenElement other) {
        app = other.app;
        leftX = other.leftX;
        upY = other.upY;
        rightX = other.rightX;
        downY = other.downY;
        z = other.z;
        scaleMode = other.scaleMode;
    }

    public abstract void draw();

    public LabyrinthApp getApp() {
        return app;
    }

    public float getLeftX() {
        return leftX;
    }

    public float getUpY() {
        return upY;
    }

    public float getRightX() {
        return rightX;
    }

    public float getDownY() {
        return downY;
    }

    public int getZ() {
        return z;
    }

    ScaleMode getScaleMode() {
        return scaleMode;
    }

    public void setSize(float leftX, float upY, float rightX, float downY) {
        this.leftX = leftX;
        this.upY = upY;
        this.rightX = rightX;
        this.downY = downY;
    }

    @Override
    public int compareTo(ScreenElement o) {
        if (o.z != z) {
            return z - o.z;
        } else if (o.leftX != leftX) {
            return leftX < o.leftX ? -1 : 1;
        } else if (o.upY != upY) {
            return upY < o.upY ? -1 : 1;
        } else if (o.rightX != rightX) {
            return rightX < o.rightX ? -1 : 1;
        } else if (o.downY != downY) {
            return downY < o.downY ? -1 : 1;
        }
        return hashCode()-o.hashCode();
    }
}
