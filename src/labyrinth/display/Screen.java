package labyrinth.display;

import labyrinth.LabyrinthApp;
import labyrinth.menu.Interactive;
import labyrinth.menu.InteractiveEvent;
import labyrinth.menu.MousePress;

import java.util.HashSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Screen extends ScreenElement implements Interactive {

    private TreeSet<ScreenElement> elements;
    private TreeSet<ScreenElement> newElements;
    private HashSet<ScreenElement> elementsToRemove;
    private float scalingFactorX;
    private float scalingFactorY;
    private float scalingFactorMin;
    private float shiftX;
    private float shiftY;

    public Screen(LabyrinthApp app, float leftX, float upY, float rightX, float downY, int z) {
        super(app, leftX, upY, rightX, downY, z, ScaleMode.NONE);
        elements = new TreeSet<>();
        newElements = new TreeSet<>();
        elementsToRemove = new HashSet<>();
    }

    public void loadElement(ScreenElement element, boolean replace) {
        if (replace) {
            elements = elements.stream()
                    .filter(e -> e.getLeftX() < element.getLeftX()
                            || e.getRightX() > element.getRightX()
                            || e.getUpY() < element.getUpY()
                            || e.getDownY() > element.getDownY())
                    .collect(Collectors.toCollection(TreeSet::new));
        }
        newElements.add(element);
    }

    public void loadElement(ScreenElement element) {
        loadElement(element, false);
    }

    public void removeElement(ScreenElement element) {
        elementsToRemove.add(element);
    }

    private InteractiveEvent transformEvent(InteractiveEvent event, ScreenElement child) {
        if (!(event instanceof MousePress)) {
            return event;
        }

        MousePress mousePress = (MousePress) event;

        ScaleMode scaleMode = child.getScaleMode();

        if (scaleMode == ScaleMode.NONE) {
            return mousePress;
        }

        calculateScaling();

        float x = mousePress.getMouseX(), y = mousePress.getMouseY();
        float leftX = getLeftX(), upY = getUpY(), rightX = getRightX(), downY = getDownY();

        float scaleX = (scaleMode == ScaleMode.MIN) ? scalingFactorMin : scalingFactorX;
        float scaleY = (scaleMode == ScaleMode.MIN) ? scalingFactorMin : scalingFactorY;

        x -= (leftX+rightX - shiftX*scaleX)/2;
        y -= (upY+downY - shiftY*scaleY)/2;
        x /= scaleX;
        y /= scaleY;

        return new MousePress(mousePress.getEventType(), mousePress.getMouseButton(), x, y);
    }

    @Override
    public void draw() {

        elements.addAll(newElements);
        newElements.clear();
        elements.removeAll(elementsToRemove);
        elementsToRemove.clear();

        if (elements.size() == 0) {
            return;
        }

        calculateScaling();

        LabyrinthApp app = getApp();

        float leftX = getLeftX(), rightX = getRightX(), upY = getUpY(), downY = getDownY();

        for (ScreenElement element : elements) {
            if (element.getScaleMode() == ScaleMode.NONE) {
                element.draw();
            } else {
                float scaleX, scaleY;
                if (element.getScaleMode() == ScaleMode.MIN) {
                    scaleX = scalingFactorMin;
                    scaleY = scalingFactorMin;
                } else {
                    scaleX = scalingFactorX;
                    scaleY = scalingFactorY;
                }
                app.translate((leftX+rightX - shiftX*scaleX)/2,
                        (upY+downY - shiftY*scaleY)/2);
                app.scale(scaleX, scaleY);
                element.draw();
                app.resetMatrix();
            }
        }
    }

    private void calculateScaling() {
        if (elements.size() == 0) {
            return;
        }

        ScreenElement firstElement = elements.first();

        float minLeftX = firstElement.getLeftX();
        float maxRightX = firstElement.getRightX();
        float minUpY = firstElement.getUpY();
        float maxDownY = firstElement.getDownY();

        for (ScreenElement element : elements) {
            if (element.getScaleMode() == ScaleMode.NONE) {
                continue;
            }

            if (element.getLeftX() < minLeftX) {
                minLeftX = element.getLeftX();
            }
            if (element.getRightX() > maxRightX) {
                maxRightX = element.getRightX();
            }
            if (element.getUpY() < minUpY) {
                minUpY = element.getUpY();
            }
            if (element.getDownY() > maxDownY) {
                maxDownY = element.getDownY();
            }
        }

        float leftX = getLeftX(), rightX = getRightX(), upY = getUpY(), downY = getDownY();

        scalingFactorX = (rightX-leftX)/(maxRightX-minLeftX);
        scalingFactorY = (downY-upY)/(maxDownY-minUpY);
        scalingFactorMin = Math.min(scalingFactorX, scalingFactorY);
        shiftX = minLeftX+maxRightX;
        shiftY = minUpY+maxDownY;
    }

    public void resizeScreen(float widthFactor, float heightFactor) {
        super.setSize(getLeftX()*widthFactor, getUpY()*heightFactor,
                getRightX()*widthFactor, getDownY()*heightFactor);
        for (ScreenElement element : elements) {
            if (element instanceof Screen) {
                ((Screen) element).resizeScreen(widthFactor, heightFactor);
            }
        }
    }

    @Override
    public void executeEvent(InteractiveEvent event) {
        for (ScreenElement element : elements) {
            if (element instanceof Interactive) {
                ((Interactive) element).executeEvent(transformEvent(event, element));
            }
        }
    }
}
