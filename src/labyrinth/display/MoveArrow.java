package labyrinth.display;

import labyrinth.Coordinate;
import labyrinth.Direction;
import labyrinth.LabyrinthApp;

public class MoveArrow extends ScreenElement {
    private Direction direction;
    private int color;

    static MoveArrow of(Coordinate position, Direction direction, LabyrinthApp app,
                            float paddingX, float paddingY, int color) {
        Coordinate target = direction.getTarget(position);

        int leftX = LabyrinthApp.min(position.getX(), target.getX());
        int rightX = LabyrinthApp.max(position.getX(), target.getX()) + 1;
        int upY = LabyrinthApp.min(position.getY(), target.getY());
        int downY = LabyrinthApp.max(position.getY(), target.getY()) + 1;

        return new MoveArrow(app, leftX+1+paddingX, upY+paddingY,
                rightX+1+paddingX, downY+paddingY, direction, color);
    }

    private MoveArrow(LabyrinthApp app, float leftX, float upY, float rightX, float downY,
                          Direction direction, int color) {
        super(app, leftX*10, upY*10, rightX*10, downY*10, 20, ScaleMode.MIN);
        this.direction = direction;
        this.color = color;
    }

    @Override
    public void draw() {
        LabyrinthApp app = getApp();

        app.noStroke();
        app.fill(color, 63);

        float shift = 20/3f;
        float arrowLength = 20-2*shift;
        float arrowWidth = 1;
        float tipLength = 1;
        float tipHeight = arrowWidth*2;

        float x1=0, x2=0, y1=0, y2=0;

        switch(direction) {
            case LEFT:
                x1 = getLeftX() + shift;
                x2 = x1 - tipLength;
                y1 = getUpY() + 5 - tipHeight;
                y2 = getUpY() + 5 + tipHeight;
                break;
            case RIGHT:
                x1 = getRightX() - shift;
                x2 = x1 + tipLength;
                y1 = getUpY() + 5 - tipHeight;
                y2 = getUpY() + 5 + tipHeight;
                break;
            case UP:
                y1 = getUpY() + shift;
                y2 = y1 - tipLength;
                x1 = getLeftX() + 5 - tipHeight;
                x2 = getLeftX() + 5 + tipHeight;
                break;
            case DOWN:
                y1 = getDownY() - shift;
                y2 = y1 + tipLength;
                x1 = getLeftX() + 5 - tipHeight;
                x2 = getLeftX() + 5 + tipHeight;
                break;
        }

        if (direction == Direction.LEFT || direction == Direction.RIGHT) {
            app.rect(getLeftX() + shift, getUpY() + 5 - arrowWidth/2, arrowLength, arrowWidth);
            app.triangle(x1, y1, x1, y2, x2, (y1+y2)/2);
        } else {
            app.rect(getLeftX() + 5 - arrowWidth/2, getUpY() + shift, arrowWidth, arrowLength);
            app.triangle(x1, y1, x2, y1, (x1+x2)/2, y2);
        }
    }
}
