package labyrinth.display;

import labyrinth.LabyrinthApp;

class LabyrinthGrid extends ScreenElement {
    private int gridWidth;
    private int gridHeight;
    private float lineWeight;
    private float paddingX;
    private float paddingY;

    LabyrinthGrid(LabyrinthApp app, int gridWidth, int gridHeight, float lineWeight,
                           float paddingX, float paddingY) {
        super(app, 0, 0,
                (paddingX*2+gridWidth+2)*10, (paddingY*2+gridHeight)*10,
                0, ScaleMode.MIN);

        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
        this.lineWeight = lineWeight;
        this.paddingX = paddingX;
        this.paddingY = paddingY;
    }

    public void draw() {
        LabyrinthApp app = getApp();

        float leftX = (paddingX+1)*10;
        float upY = paddingY;
        float rightX = (paddingX+gridWidth+1)*10;
        float downY = (paddingY+gridHeight)*10;

        // Background
        app.noStroke();
        app.fill(255);
        app.rect(leftX, upY, rightX-leftX, downY-upY);
        app.rect(leftX-10, upY, 10, 10);
        app.rect(rightX, downY-10, 10, 10);

        // Grid

        app.stroke(127);
        app.strokeWeight(lineWeight*10);
        app.noFill();

        // horizontal
        for (int row = 0; row <= gridHeight; row++) {
            app.line(leftX, upY+row*10, rightX, upY+row*10);
        }
        // vertical
        for (int col = 0; col <= gridWidth; col++) {
            app.line(leftX+col*10, upY, leftX+col*10, downY);
        }
    }
}
