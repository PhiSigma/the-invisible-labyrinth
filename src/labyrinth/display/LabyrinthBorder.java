package labyrinth.display;

import labyrinth.LabyrinthApp;

class LabyrinthBorder extends ScreenElement {
    private int gridWidth;
    private int gridHeight;
    private float lineWeight;
    private float paddingX;
    private float paddingY;

    LabyrinthBorder(LabyrinthApp app, int gridWidth, int gridHeight, float lineWeight,
                           float paddingX, float paddingY) {
        super(app, 0, 0,
                (paddingX*2+gridWidth+2)*10, (paddingY*2+gridHeight)*10,
                2, ScaleMode.MIN);

        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
        this.lineWeight = lineWeight;
        this.paddingX = paddingX;
        this.paddingY = paddingY;
    }

    public void draw() {
        LabyrinthApp app = getApp();

        float leftX = (paddingX+1)*10;
        float upY = paddingY*10;
        float rightX = (paddingX+gridWidth+1)*10;
        float downY = (paddingY+gridHeight)*10;

        app.stroke(0);
        app.strokeWeight(lineWeight*10);
        app.noFill();

        // Horizontal (top)
        app.line(leftX-10, upY, rightX, upY);
        app.line(leftX-10, upY+10, leftX, upY+10);
        // Horizontal (bottom)
        app.line(leftX, downY, rightX+10, downY);
        app.line(rightX, downY-10, rightX+10, downY-10);
        // Vertical
        app.line(leftX, upY+10, leftX, downY);
        app.line(rightX, upY, rightX, downY-10);
    }
}
