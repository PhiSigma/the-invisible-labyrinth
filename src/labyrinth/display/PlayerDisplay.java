package labyrinth.display;

import labyrinth.LabyrinthApp;
import labyrinth.Player;

public class PlayerDisplay extends ScreenElement {

    private Player player;
    private float originalShiftX;
    private float originalShiftY;
    private float shiftX;
    private float shiftY;

    PlayerDisplay(LabyrinthApp app, float leftX, float upY, float rightX, float downY, Player player,
                         float shiftX, float shiftY) {
        super(app, leftX, upY, rightX, downY, 4, ScaleMode.MIN);
        this.player = player;
        this.originalShiftX = shiftX;
        this.originalShiftY = shiftY;
        this.shiftX = shiftX;
        this.shiftY = shiftY;
    }

    PlayerGhost makeGhost() {
        return new PlayerGhost(getApp(), getLeftX(), getUpY(), getRightX(), getDownY(), getPlayer(),
                shiftX, shiftY, this);
    }

    void drawPlayer(int alpha) {
        float playerSize = getApp().getSettings().getPlayerSize();
        LabyrinthApp app = getApp();
        app.ellipseMode(LabyrinthApp.CENTER);
        app.fill(player.getColor(), alpha);
        app.noStroke();
        app.ellipse(getX(), getY(), playerSize, playerSize);
    }

    void drawNumber(int n) {
        float x = getX();
        float y = getY();
        float circleSize = getApp().getSettings().getPlayerSize() / 5;
        LabyrinthApp app = getApp();

        app.ellipseMode(LabyrinthApp.CENTER);
        app.fill(255);
        app.noStroke();

        if (n % 2 == 1) {
            app.ellipse(x, y, circleSize, circleSize);
        }
        if (n / 2 > 0) {
            app.ellipse(x+circleSize*1.1f, y-circleSize*1.1f, circleSize, circleSize);
            app.ellipse(x-circleSize*1.1f, y+circleSize*1.1f, circleSize, circleSize);
        }
        if (n / 2 > 1) {
            app.ellipse(x+circleSize*1.1f, y+circleSize*1.1f, circleSize, circleSize);
            app.ellipse(x-circleSize*1.1f, y-circleSize*1.1f, circleSize, circleSize);
        }
        if (n / 2 > 2) {
            app.ellipse(x+circleSize*1.1f, y, circleSize, circleSize);
            app.ellipse(x-circleSize*1.1f, y, circleSize, circleSize);
        }
        if (n / 2 > 3) {
            app.ellipse(x, y-circleSize*1.1f, circleSize, circleSize);
            app.ellipse(x, y+circleSize*1.1f, circleSize, circleSize);
        }
    }

    @Override
    public void draw() {
        drawPlayer(255);


        /*app.textSize(playerSize*2/3);
        app.fill(255);
        app.textAlign(LabyrinthApp.CENTER, LabyrinthApp.CENTER);*/

        if (player.getGame().isTurn(player)) {
            drawNumber(player.getGame().getRemainingMoves());
            //app.text(player.getGame().getRemainingMoves(), getX(), getY());
        }
    }

    float getX() {
        return (player.getPosition().getX() + shiftX + 0.5f)*10;
    }

    float getY() {
        return (player.getPosition().getY() + shiftY + 0.5f)*10;
    }

    Player getPlayer() {
        return player;
    }

    void shiftX(float x) {
        shiftX += x;
    }

    void shiftY(float y) {
        shiftY += y;
    }

    void resetShift() {
        shiftX = originalShiftX;
        shiftY = originalShiftY;
    }

    boolean shiftIsReset() {
        return shiftX == originalShiftX && shiftY == originalShiftY;
    }

    void swapShift(PlayerDisplay other) {
        float tempX = shiftX;
        float tempY = shiftY;
        shiftX = other.shiftX;
        shiftY = other.shiftY;
        other.shiftX = tempX;
        other.shiftY = tempY;
    }
}
