package labyrinth.display;

import labyrinth.LabyrinthApp;

public class FlatBackground extends ScreenElement {
    private int color;

    public FlatBackground(LabyrinthApp app, float leftX, float upY,
                          float rightX, float downY, int color) {
        super(app, leftX, upY, rightX, downY, -100, ScaleMode.BOTH);
        this.color = color;
    }

    @Override
    public void draw() {
        LabyrinthApp app = getApp();
        app.noStroke();
        app.fill(color);
        app.rect(getLeftX(), getUpY(), getRightX()-getLeftX(), getDownY()-getUpY());
    }

    public void setColor(int color) {
        this.color = color;
    }
}
