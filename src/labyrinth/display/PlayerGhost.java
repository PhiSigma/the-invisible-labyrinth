package labyrinth.display;

import labyrinth.LabyrinthApp;
import labyrinth.Player;

public class PlayerGhost extends PlayerDisplay {
    private boolean isVisible;
    private PlayerDisplay parent;

    PlayerGhost(LabyrinthApp app, float leftX, float upY, float rightX, float downY, Player player,
                float shiftX, float shiftY, PlayerDisplay parent) {
        super(app, leftX, upY, rightX, downY, player, shiftX, shiftY);
        isVisible = false;
        this.parent = parent;
    }

    @Override
    public void draw() {
        if (!isVisible) {
            return;
        }

        super.drawPlayer(63);

        LabyrinthApp app = getApp();
        app.strokeWeight(getApp().getSettings().getPlayerSize()/3);
        app.stroke(getPlayer().getColor(), 31);
        app.line(getX(), getY(), parent.getX(), parent.getY());

        super.drawNumber(getPlayer().getDiceRoll());
    }

    @Override
    float getX() {
        return super.getX() + 10*(getPlayer().getLastPosition().getX()-getPlayer().getPosition().getX());
    }

    float getY() {
        return super.getY() + 10*(getPlayer().getLastPosition().getY()-getPlayer().getPosition().getY());
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }
}
