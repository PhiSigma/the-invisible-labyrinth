package labyrinth;

public enum Direction {
    LEFT(-1, 0),
    UP(0, -1),
    RIGHT(1, 0),
    DOWN(0, 1),
    ;

    private int x;
    private int y;

    Direction(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Direction getMove(Coordinate position, Coordinate target) {
        for (Direction direction : Direction.values()) {
            if (direction.getTarget(position).equals(target)) {
                return direction;
            }
        }
        return null;
    }

    public Coordinate getTarget(Coordinate position) {
        return new Coordinate(position.getX()+x, position.getY()+y);
    }

    public Wall getWall(Coordinate position) {
        return new Wall(
                new Coordinate(
                        position.getX() + (x+1)/2,
                        position.getY() + (y+1)/2
                ),
                y != 0
        );
    }
}
