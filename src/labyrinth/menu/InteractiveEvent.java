package labyrinth.menu;

public abstract class InteractiveEvent {
    private final EventType eventType;

    InteractiveEvent(EventType eventType) {
        this.eventType = eventType;
    }

    public EventType getEventType() {
        return eventType;
    }
}
