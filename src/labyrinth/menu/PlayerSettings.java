package labyrinth.menu;

import labyrinth.LabyrinthApp;
import labyrinth.Settings;
import labyrinth.display.ScaleMode;
import labyrinth.display.ScreenElement;
import labyrinth.display.menu.TextDisplay;
import labyrinth.display.menu.TextFieldDisplay;
import processing.core.PConstants;

public class PlayerSettings extends ScreenElement implements Interactive {
    private int index;
    private boolean isAI;
    private TextFieldDisplay indexText;
    private Checkbox aiBox;
    private Slider aiSlider;
    private TextDisplay aiText;
    private ColorPicker colorPicker;

    public PlayerSettings(LabyrinthApp app, float leftX, float upY, float rightX, float downY, int index) {
        super(app, leftX, upY, rightX, downY, 3, ScaleMode.MIN);
        this.index = index;

        final float width = rightX-leftX, height = downY-upY,
                centerX = (rightX+leftX)/2, centerY = (downY+upY)/2;
        final float tileSize = Math.min(height, width/4);
        final Settings settings = app.getSettings();

        final int color = settings.getColor(index);
        indexText = new TextFieldDisplay(app,
                centerX - 1.8f*tileSize, centerY - 0.3f*tileSize,
                centerX - 1.2f*tileSize, centerY + 0.3f*tileSize,
                Integer.toString(index+1));
        indexText.setTextColor(app.color(255));
        indexText.setColor(color);

        app.colorMode(PConstants.HSB, 360, 100, 100);
        colorPicker = new ColorPicker(app,
                centerX - 0.5f*tileSize, centerY, 0.48f*tileSize, 0.2f,
                this::changeColor, app.hue(color), app.saturation(color), app.brightness(color));
        app.colorMode(PConstants.RGB, 255, 255, 255);

        isAI = ! settings.isHuman(index);
        aiBox = new Checkbox(app, centerX, centerY-tileSize/2,
                centerX+tileSize*2, centerY-tileSize/6, "AI",
                this::setAI, isAI);
        aiText = new TextDisplay(app, centerX, centerY-tileSize/6,
                centerX+tileSize*2, centerY+tileSize/6, "Strength");
        aiSlider = new Slider(app, centerX, centerY+tileSize/6,
                centerX+tileSize*1.8f, centerY+tileSize/2, 0.5f,
                this::setStrength, settings.getStrength(index));
    }

    @Override
    public void draw() {
        indexText.draw();
        aiBox.draw();
        colorPicker.draw();
        if (isAI) {
            aiText.draw();
            aiSlider.draw();
        }
    }

    @Override
    public void executeEvent(InteractiveEvent event) {
        aiBox.executeEvent(event);
        aiSlider.executeEvent(event);
        colorPicker.executeEvent(event);
    }

    private void changeColor(int color) {
        indexText.setColor(color);
        getApp().getSettings().setColor(index, color);
    }

    private void setAI(boolean isAI) {
        this.isAI = isAI;
        getApp().getSettings().setHuman(index, !isAI);
    }

    public void externalSetAI(boolean isAI) {
        setAI(isAI);
        aiBox.setValue(isAI);
    }

    private void setStrength(float strength) {
        getApp().getSettings().setStrength(index, strength);
    }

    public void externalSetStrength(float strength) {
        setStrength(strength);
        aiSlider.setValue(strength);
    }
}
