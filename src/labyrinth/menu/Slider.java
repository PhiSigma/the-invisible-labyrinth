package labyrinth.menu;

import labyrinth.LabyrinthApp;
import labyrinth.display.menu.HuePickerDisplay;
import labyrinth.display.ScreenElement;
import labyrinth.display.menu.SliderDisplay;

import java.util.function.Consumer;

public class Slider extends ElementWithDisplay<SliderDisplay> implements Interactive {
    private Consumer<Float> function;
    private boolean beingDragged;

    Slider(SliderDisplay display, Consumer<Float> function, float defaultValue) {
        super(display);
        this.function = function;
        beingDragged = false;
        apply(defaultValue);
    }

    public Slider (LabyrinthApp app, float leftX, float upY, float rightX, float downY,
                   float thicknessFactor, Consumer<Float> function, float defaultValue) {
        this(new SliderDisplay(app, leftX, upY, rightX, downY, thicknessFactor), function, defaultValue);
    }

    public void setValue(float value) {
        getDisplay().setValue(value);
    }

    private void apply(float value) {
        function.accept(value);
        setValue(value);
    }

    @Override
    public void executeEvent(InteractiveEvent event) {ScreenElement displayElement = getDisplay();
        final SliderDisplay display = getDisplay();

        if (! (event instanceof MousePress)) {
            return;
        }
        MousePress mousePress = (MousePress) event;
        if (mousePress.getMouseButton() != LabyrinthApp.LEFT) {
            return;
        }
        EventType type = mousePress.getEventType();
        switch(type) {
            case MOUSERELEASED:
                beingDragged = false;
                return;
            case MOUSEPRESSED:
                if (display.isInside(mousePress)) {
                    beingDragged = true;
                }
            case MOUSEDRAGGED:
                if (beingDragged) {
                    apply(display.calculateValue(mousePress));
                }
        }
    }
}
