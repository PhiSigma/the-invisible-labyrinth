package labyrinth.menu;

public class MousePress extends InteractiveEvent {
    private final int mouseButton;
    private final float mouseX;
    private final float mouseY;

    public MousePress(EventType eventType, int mouseButton, float mouseX, float mouseY) {
        super(eventType);
        if (eventType.isInvalid(this)) {
            throw new IllegalArgumentException("invalid event type");
        }
        this.mouseButton = mouseButton;
        this.mouseX = mouseX;
        this.mouseY = mouseY;
    }

    public int getMouseButton() {
        return mouseButton;
    }

    public float getMouseX() {
        return mouseX;
    }

    public float getMouseY() {
        return mouseY;
    }
}
