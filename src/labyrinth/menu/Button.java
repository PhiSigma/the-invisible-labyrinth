package labyrinth.menu;

import labyrinth.LabyrinthApp;
import labyrinth.display.menu.ButtonDisplay;
import labyrinth.display.ScreenElement;

public class Button extends ElementWithDisplay<ScreenElement> implements Interactive {
    private Runnable function;

    public Button(LabyrinthApp app, float leftX, float upY, float rightX, float downY,
                  String text, Runnable function) {
        super(new ButtonDisplay(app, leftX, upY, rightX, downY, text));
        this.function = function;
    }

    @Override
    public void executeEvent(InteractiveEvent event) {
        if (event.getEventType() != EventType.MOUSECLICKED) {
            return;
        }

        MousePress mouseClick = (MousePress) event;

        final float leftX = getLeftX(), upY = getUpY(), rightX = getRightX(), downY = getDownY();
        final float x = mouseClick.getMouseX(), y = mouseClick.getMouseY();

        if (mouseClick.getMouseButton() == LabyrinthApp.LEFT
                && x >= leftX && x <= rightX && y >= upY && y <= downY) {
            function.run();
        }
    }
}
