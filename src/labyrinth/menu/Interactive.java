package labyrinth.menu;

public interface Interactive {
    void executeEvent(InteractiveEvent event);
}