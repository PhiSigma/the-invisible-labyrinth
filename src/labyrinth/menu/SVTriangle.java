package labyrinth.menu;

import labyrinth.LabyrinthApp;
import labyrinth.display.menu.SVTriangleDisplay;

import java.util.function.BiConsumer;

public class SVTriangle extends ElementWithDisplay<SVTriangleDisplay> implements Interactive {
    private BiConsumer<Float, Float> function;
    private boolean beingDragged;

    public SVTriangle (LabyrinthApp app, float centerX, float centerY, float radius,
                       float thicknessFactor, BiConsumer<Float, Float> function,
                       float defaultSaturation, float defaultValue) {
        super(new SVTriangleDisplay(app, centerX, centerY, radius, thicknessFactor));
        this.function = function;
        beingDragged = false;
        apply(defaultSaturation, defaultValue);
    }

    private void apply(float saturation, float value) {
        final SVTriangleDisplay display = getDisplay();
        function.accept(saturation, value);
        display.setSaturation(saturation);
        display.setValue(value);
    }

    @Override
    public void executeEvent(InteractiveEvent event) {
        final SVTriangleDisplay display = getDisplay();

        if (! (event instanceof MousePress)) {
            return;
        }
        MousePress mousePress = (MousePress) event;
        if (mousePress.getMouseButton() != LabyrinthApp.LEFT) {
            return;
        }
        EventType type = mousePress.getEventType();
        switch(type) {
            case MOUSERELEASED:
                beingDragged = false;
                return;
            case MOUSEPRESSED:
                if (display.isInside(mousePress)) {
                    beingDragged = true;
                }
            case MOUSEDRAGGED:
                if (beingDragged) {
                    apply(display.calculateSaturation(mousePress), display.calculateValue(mousePress));
                }
        }
    }

    void setHue(float hue) {
        getDisplay().setHue(hue);
    }
}
