package labyrinth.menu;

import labyrinth.display.ScreenElement;

public abstract class ElementWithDisplay<T extends ScreenElement> extends ScreenElement {
    private T display;

    ElementWithDisplay(T display) {
        super(display);
        this.display = display;
    }

    T getDisplay() {
        return display;
    }

    @Override
    public void draw() {
        display.draw();
    }

    @Override
    public void setSize(float leftX, float upY, float rightX, float downY) {
        super.setSize(leftX, upY, rightX, downY);
        display.setSize(leftX, upY, rightX, downY);
    }
}
