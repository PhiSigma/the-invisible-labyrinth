package labyrinth.menu;

import labyrinth.LabyrinthApp;
import labyrinth.display.menu.CheckboxDisplay;

import java.util.function.Consumer;

public class Checkbox extends ElementWithDisplay<CheckboxDisplay> implements Interactive {
    private Consumer<Boolean> function;
    private boolean value;

    public Checkbox(LabyrinthApp app, float leftX, float upY, float rightX, float downY,
                    String text, Consumer<Boolean> function, boolean defaultValue) {
        super(new CheckboxDisplay(app, leftX, upY, rightX, downY, text));
        this.function = function;
        apply(defaultValue);
    }

    private void apply(boolean value) {
        function.accept(value);
        setValue(value);
    }

    public void setValue(boolean value) {
        this.value = value;
        getDisplay().setValue(value);
    }

    @Override
    public void executeEvent(InteractiveEvent event) {
        if (event.getEventType() != EventType.MOUSECLICKED) {
            return;
        }

        MousePress mouseClick = (MousePress) event;

        final float leftX = getLeftX(), upY = getUpY(), rightX = getRightX(), downY = getDownY();
        final float x = mouseClick.getMouseX(), y = mouseClick.getMouseY();

        if (mouseClick.getMouseButton() == LabyrinthApp.LEFT
                && x >= leftX && x <= rightX && y >= upY && y <= downY) {
            apply(!value);
        }
    }
}
