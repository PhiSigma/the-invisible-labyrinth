package labyrinth.menu;

import labyrinth.LabyrinthApp;
import labyrinth.display.menu.HuePickerDisplay;
import labyrinth.display.ScreenElement;
import processing.core.PConstants;

import java.util.function.Consumer;

public class ColorPicker extends ScreenElement implements Interactive {
    private LabyrinthApp app;
    private Slider huePicker;
    private SVTriangle svTriangle;
    private Consumer<Integer> function;
    private float hue;
    private float saturation;
    private float value;

    public ColorPicker (LabyrinthApp app, float centerX, float centerY, float radius,
                        float thicknessFactor, Consumer<Integer> function,
                        float defaultHue, float defaultSaturation, float defaultValue) {
        super(new HuePickerDisplay(app, centerX, centerY, radius, thicknessFactor));
        this.app = app;
        svTriangle = new SVTriangle(app, centerX, centerY, radius, thicknessFactor,
                this::setSV, defaultSaturation, defaultValue);
        huePicker = new HuePicker(app, centerX, centerY, radius, thicknessFactor,
                this::setH, defaultHue);
        this.function = function;
        hue = defaultHue;
        saturation = defaultSaturation;
        value = defaultValue;
    }

    public void setH(float hue) {
        this.hue = hue;
        svTriangle.setHue(hue);
        if (function != null) {
            app.colorMode(PConstants.HSB, 360, 100, 100);
            function.accept(app.color(hue, saturation, value));
            app.colorMode(PConstants.RGB, 255, 255, 255);
        }
    }

    public void setSV(float saturation, float value) {
        this.saturation = saturation;
        this.value = value;
        if (function != null) {
            app.colorMode(PConstants.HSB, 360, 100, 100);
            function.accept(app.color(hue, saturation, value));
            app.colorMode(PConstants.RGB, 255, 255, 255);
        }
    }

    @Override
    public void draw() {
        huePicker.draw();
        svTriangle.draw();
    }

    @Override
    public void executeEvent(InteractiveEvent event) {
        huePicker.executeEvent(event);
        svTriangle.executeEvent(event);
    }
}
