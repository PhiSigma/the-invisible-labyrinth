package labyrinth.menu;

public enum EventType {
    MOUSECLICKED(MousePress.class),
    MOUSEPRESSED(MousePress.class),
    MOUSERELEASED(MousePress.class),
    MOUSEDRAGGED(MousePress.class),
    KEYPRESSED(KeyPress.class),
    KEYRELEASED(KeyPress.class),
    ;

    private Class eventClass;

    EventType(Class eventClass) {
        this.eventClass = eventClass;
    }

    boolean isInvalid(InteractiveEvent event) {
        return !eventClass.isInstance(event);
    }
}
