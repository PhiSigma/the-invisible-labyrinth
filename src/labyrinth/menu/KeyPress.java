package labyrinth.menu;

import labyrinth.LabyrinthApp;

public class KeyPress extends InteractiveEvent {
    private final char key;
    private final int keyCode;

    public KeyPress(EventType eventType, char key, int keyCode) {
        super(eventType);
        if (eventType.isInvalid(this)) {
            throw new IllegalArgumentException("invalid event type");
        }
        this.key = key;
        this.keyCode = keyCode;
    }

    public char getKey() {
        return key;
    }

    public int getKeyCode() {
        return keyCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (! (obj instanceof KeyPress)) {
            return false;
        }

        KeyPress o = (KeyPress) obj;

        if (key != o.key) {
            return false;
        }
        if (key == LabyrinthApp.CODED) {
            return keyCode == o.keyCode;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Character.hashCode(key) + (key == LabyrinthApp.CODED ? keyCode : 0);
    }
}
