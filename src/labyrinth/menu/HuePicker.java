package labyrinth.menu;

import labyrinth.LabyrinthApp;
import labyrinth.display.menu.HuePickerDisplay;

import java.util.function.Consumer;

public class HuePicker extends Slider {
    public HuePicker(LabyrinthApp app, float centerX, float centerY, float radius,
                  float thicknessFactor, Consumer<Float> function,
                  float defaultValue) {
        super(new HuePickerDisplay(app, centerX, centerY, radius, thicknessFactor), function, defaultValue);
    }
}
