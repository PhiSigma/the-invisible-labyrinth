package labyrinth.menu;

import labyrinth.LabyrinthApp;
import labyrinth.display.menu.TextFieldDisplay;

import java.util.function.Consumer;

import static processing.core.PConstants.*;

public class IntegerField extends ElementWithDisplay<TextFieldDisplay> implements Interactive {
    private Consumer<Integer> function;
    private int minValue;
    private int maxValue;
    private boolean active;

    public IntegerField(LabyrinthApp app, float leftX, float upY, float rightX, float downY,
                        Consumer<Integer> function, int defaultValue, int minValue, int maxValue) {
        super(new TextFieldDisplay(app, leftX, upY, rightX, downY,
                Integer.toString(defaultValue)));
        this.function = function;
        this.minValue = minValue;
        this.maxValue = maxValue;
        active = false;
        function.accept(defaultValue);
    }

    public void setValue(int i) {
        getDisplay().setText(Integer.toString(i));
        parseText(Integer.toString(i));
    }

    private void parseText(String text) {
        final TextFieldDisplay display = getDisplay();

        try {
            int value = Integer.parseInt(text);
            if (value >= minValue && value <= maxValue) {
                function.accept(value);
                display.resetColor();
                return;
            }
        } catch (Exception ignore) {

        }

        display.setColor(getApp().color(255, 128, 128));
    }

    @Override
    public void executeEvent(InteractiveEvent event) {
        final TextFieldDisplay display = getDisplay();

        if (event.getEventType() == EventType.MOUSEPRESSED) {
            MousePress mouseClick = (MousePress) event;
            if (mouseClick.getMouseButton() != LEFT) {
                return;
            }

            final float leftX = getLeftX(), upY = getUpY(), rightX = getRightX(), downY = getDownY();
            final float x = mouseClick.getMouseX(), y = mouseClick.getMouseY();

            active = x >= leftX && x <= rightX && y >= upY && y <= downY;
            if (active) {
                display.setStrokeWeightFactor(0.05f);
            } else {
                display.resetStrokeWeightFactor();
            }
        } else if (event.getEventType() == EventType.KEYRELEASED && active) {
            String text = display.getText();
            char key = ((KeyPress) event).getKey();
            switch (key) {
                case ENTER:
                case TAB:
                case RETURN:
                case ESC:
                    break;
                case BACKSPACE:
                case DELETE:
                    if (text.length() > 0) {
                        text = text.substring(0, text.length() - 1);
                    }
                    break;
                default:
                    text += key;
            }

            display.setText(text);
            parseText(text);
        }
    }
}
