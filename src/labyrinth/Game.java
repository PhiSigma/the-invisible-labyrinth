package labyrinth;

import labyrinth.controller.AIPlayer;
import labyrinth.controller.Action;
import labyrinth.display.LabyrinthScreen;

import java.util.ArrayList;

public class Game {
    private LabyrinthStructure structure;
    private LabyrinthScreen screen;
    private boolean gameOver;
    private ArrayList<Player> players;
    private int turn;
    private int diceRoll;
    private boolean turnStarted;
    private ArrayList<Direction> moves;

    Game(LabyrinthApp app, int gridWidth, int gridHeight, float wallFactor) {
        structure = new LabyrinthStructure(gridWidth, gridHeight, wallFactor);
        screen = new LabyrinthScreen(app, 0, 0, app.width, app.height, 0,
                gridWidth, gridHeight, structure.getWalls(), app.getSettings().getLineWeightWall(),
                0.005f, 0, 0);
        gameOver = false;
        players = new ArrayList<>();
        turn = 0;
        turnStarted = false;
        moves = new ArrayList<>();
        app.load(screen);
    }

    void start() {
        ArrayList<Player> shuffledPlayers = new ArrayList<>();
        while (players.size() > 0) {
            int i = (int) (Math.random()*players.size());
            shuffledPlayers.add(players.get(i));
            players.remove(i);
        }
        players = shuffledPlayers;

        if (players.size() > turn) {
            Player player = players.get(turn);
            screen.showTurn(player, true);
            if (!screen.getApp().getSettings().isPreviewingMoves()
                    || player.getController() instanceof AIPlayer) {
                startTurn();
            }
        }

        if (screen.getApp().getSettings().isReallyShowingAIColors()) {
            for (Player player : players) {
                if (player.getController() instanceof AIPlayer) {
                    AIPlayer controller = (AIPlayer) player.getController();
                    screen.makeColoredWalls(controller, structure.getAllPossibleWalls());
                    break;
                }
            }
        }
    }

    void addPlayer(Player player) {
        players.add(player);
        player.setGame(this);
        player.setPosition(structure.getStart());
        screen.addPlayer(player);
    }

    void update() {
        if (players.size() > 0 && !gameOver) {
            ArrayList<Action> actions = players.get(turn).getController().askInput();
            for (Action action : actions) {
                perform(action);
            }
        }
    }

    private void perform(Action action) {
        switch(action) {
            case START:
                startTurn();
                break;
            case UP:
                addMove(Direction.UP);
                break;
            case LEFT:
                addMove(Direction.LEFT);
                break;
            case RIGHT:
                addMove(Direction.RIGHT);
                break;
            case DOWN:
                addMove(Direction.DOWN);
                break;
            case END:
                endTurn();
                break;
            case BACK:
                undoLastMove();
                break;
        }
    }

    private void rollDie() {
        Player player = players.get(turn);
        diceRoll = 1 + (int) (Math.random() * screen.getApp().getSettings().getDieSize());
        player.setDiceRoll(diceRoll);
    }

    private void startTurn() {
        if (!turnStarted && players.size() > 0) {
            Player player = players.get(turn);
            rollDie();
            screen.hideGhost(player);
            moves = new ArrayList<>();
            turnStarted = true;
        }
    }

    private void addMove(Direction move) {
        if (turnStarted && getRemainingMoves() > 0) {
            if (screen.getApp().getSettings().isPreviewingMoves()
                    && !(players.get(turn).getController() instanceof AIPlayer)) {
                previewMove(move);
            }
            moves.add(move);
        }
    }

    private void previewMove(Direction move) {
        Coordinate position = players.get(turn).getPosition();
        boolean insideBounds = true;
        for (Direction otherMove : moves) {
            if (! structure.insideBounds(position)) {
                break;
            }
            Coordinate target = otherMove.getTarget(position);
            if (structure.insideBounds(target)) {
                position = target;
            } else {
                insideBounds = false;
            }
        }
        if (insideBounds && structure.insideBounds(move.getTarget(position))) {
            screen.previewMove(players.get(turn), position, move);
        }
    }

    private void undoLastMove() {
        if (moves.size() > 0) {
            moves.remove(moves.size()-1);
            if (screen.getApp().getSettings().isPreviewingMoves()) {
                screen.removePreview(moves.size());
            }
        }
    }

    private void endTurn() {
        if (getRemainingMoves() == 0 && players.size() > 0) {
            applyMove();
            if (gameOver) {
                screen.showTurn(players.get(turn),
                        players.get(turn).getController() instanceof AIPlayer);
                return;
            }
            turn = (turn + 1) % players.size();
            turnStarted = false;
            Player nextPlayer = players.get(turn);
            screen.showTurn(nextPlayer,
                    !(nextPlayer.getController() instanceof AIPlayer));
            if (!screen.getApp().getSettings().isPreviewingMoves()
                    || nextPlayer.getController() instanceof AIPlayer) {
                startTurn();
            }
        }
    }

    public int getRemainingMoves() {
        return diceRoll-moves.size();
    }

    public boolean isTurn(Player player) {
        return turnStarted && players.indexOf(player) == turn;
    }

    private void applyMove() {
        Player player = players.get(turn);
        Coordinate originalPosition = player.getPosition();
        Coordinate position = originalPosition;

        for (Direction direction : moves) {
            if (structure.canGo(position, direction)) {
                position = direction.getTarget(position);
                if (position.equals(structure.getFinish())) {
                    gameOver = true;
                    if (!screen.getApp().getSettings().isReallyShowingAIColors()) {
                        screen.showWalls();
                    }
                    break;
                }
            } else {
                break;
            }
        }

        player.setPosition(position);
        screen.showGhost(player);

        for (Player otherPlayer : players) {
            otherPlayer.getController().log(player, diceRoll, originalPosition, position);
        }
    }

    int getPlayerCount() {
        return players.size();
    }

    public boolean insideBounds(Coordinate coordinate) {
        return structure.insideBounds(coordinate);
    }

    public int getFinishDistance(Coordinate coordinate) {
        return structure.getFinishDistance(coordinate);
    }

    public float getWallFactor() {
        return structure.getWallFactor();
    }

    boolean isGameOver() {
        return gameOver;
    }
}