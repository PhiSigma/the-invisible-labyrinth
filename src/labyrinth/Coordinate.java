package labyrinth;

public class Coordinate {
    private int x;
    private int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object obj) {
        if (! (obj instanceof Coordinate)) {
            return false;
        }
        Coordinate o = (Coordinate) obj;
        return o.x == x && o.y == y;
    }

    @Override
    public int hashCode() {
        return x*32 + y;
    }
}
