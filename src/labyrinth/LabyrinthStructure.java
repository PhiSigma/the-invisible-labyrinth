package labyrinth;

import java.util.ArrayList;
import java.util.HashSet;

class LabyrinthStructure {
    private int gridWidth;
    private int gridHeight;
    private float wallFactor;
    private HashSet<Wall> walls;

    LabyrinthStructure(int gridWidth, int gridHeight, float wallFactor) {
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
        this.wallFactor = wallFactor;
        this.walls = initialize(wallFactor);
        makeAccessible();
        removeFourBlocks();
    }

    HashSet<Wall> getWalls() {
        return walls;
    }

    Coordinate getStart() {
        return new Coordinate(0, 0);
    }

    Coordinate getFinish() {
        return new Coordinate(gridWidth-1, gridHeight-1);
    }

    private HashSet<Wall> initialize(float wallFactor) {
        HashSet<Wall> walls = new HashSet<>();
        for (Wall wall :getAllPossibleWalls()) {
            if (Math.random() < wallFactor) {
                walls.add(wall);
            }
        }
        return walls;
    }

    private void makeAccessible() {
        HashSet<Coordinate> accessible = getAccessible();
        while (accessible.size() < gridWidth*gridHeight) {
            removeBorderWall(accessible);
            accessible = getAccessible();
        }
    }

    private void removeBorderWall(HashSet<Coordinate> accessible) {
        ArrayList<Wall> borderWalls = new ArrayList<Wall>();
        for (Coordinate coordinate : accessible) {
            for (Direction direction : Direction.values()) {
                Coordinate target = direction.getTarget(coordinate);
                if (! accessible.contains(target) && insideBounds(target)) {
                    if (target.getX() < 0 || target.getY() < 0
                            || target.getX() >= gridWidth || target.getY() >= gridHeight) {
                        continue;
                    }
                    borderWalls.add(direction.getWall(coordinate));
                }
            }
        }
        int i = (int) (Math.random()*borderWalls.size());
        walls.remove(borderWalls.get(i));
    }

    boolean insideBounds(Coordinate position) {
        return position.getX() >= 0 && position.getY() >= 0
                && position.getX() < gridWidth && position.getY() < gridHeight;
    }

    boolean canGo(Coordinate position, Direction direction) {
        return insideBounds(direction.getTarget(position))
                && ! walls.contains(direction.getWall(position));
    }

    private HashSet<Coordinate> getAccessible() {
        HashSet<Coordinate> accessible = new HashSet<Coordinate>();
        getAccessible(new Coordinate(0, 0), accessible);
        return accessible;
    }

    private void getAccessible(Coordinate position, HashSet<Coordinate> accessible) {
        if (! accessible.contains(position)) {
            accessible.add(position);
            for (Coordinate neighbor : getNeighbors(position)) {
                getAccessible(neighbor, accessible);
            }
        }
    }

    private ArrayList<Coordinate> getNeighbors(Coordinate position) {
        ArrayList<Coordinate> neighbors = new ArrayList<Coordinate>();
        for (Direction direction : Direction.values()) {
            if (canGo(position, direction)) {
                neighbors.add(direction.getTarget(position));
            }
        }
        return neighbors;
    }

    private void removeFourBlocks() {
        for (int row = 1; row < gridHeight; row++) {
            for (int col = 1; col < gridWidth; col++) {
                if (isFourBlock(row, col)) {
                    if (Math.random() < 0.5) {
                        walls.add(new Wall(new Coordinate(
                                col - (int) (2*Math.random()), row), true));
                    } else {
                        walls.add(new Wall(new Coordinate(
                                col, row - (int) (2*Math.random())), false));
                    }
                }
            }
        }
    }

    private boolean isFourBlock(int row, int col) {
        return ! walls.contains(new Wall(new Coordinate(col-1, row), true))
                && ! walls.contains(new Wall(new Coordinate(col, row-1), false))
                && ! walls.contains(new Wall(new Coordinate(col, row), true))
                && ! walls.contains(new Wall(new Coordinate(col, row), false));
    }

    int getFinishDistance(Coordinate position) {
        return Math.abs(position.getX()-getFinish().getX())
                + Math.abs(position.getY()-getFinish().getY());
    }

    float getWallFactor() {
        return wallFactor;
    }

    HashSet<Wall> getAllPossibleWalls() {
        HashSet<Wall> possibleWalls = new HashSet<>();
        for (int row = 0; row < gridHeight; row++) {
            for (int col = 0; col < gridWidth; col++) {
                Coordinate position = new Coordinate(col, row);
                if (col < gridWidth-1) {
                    possibleWalls.add(Direction.RIGHT.getWall(position));
                }
                if (row < gridHeight-1) {
                    possibleWalls.add(Direction.DOWN.getWall(position));
                }
            }
        }
        return possibleWalls;
    }
}
